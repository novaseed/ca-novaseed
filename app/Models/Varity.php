<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str as Str;
use Illuminate\Database\Eloquent\SoftDeletes;


class Varity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;
    protected $fillable = [
        'mother_id','father_id','code','year','name','berries','crossing',
        'typecrossing','observation','parent_id','user_id','market_id','slug',
        'marketold_id','mapout_id','berries_objective','status','polem_save',
        'glasses','greenhouse_id','type','sex',
        'selected_status', 'potential_usage', 'used_in_cross', 'stage_of_development',
        'five_star_rating',
    ];

    public function mother()
    {
        return $this->belongsTo(Varity::class,'mother_id');
    }

    public function father()
    {
        return $this->belongsTo(Varity::class,'father_id');
    }

    public function mapout()
    {
        return $this->belongsTo(Mapout::class,'mapout_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function marketold()
    {
        return $this->belongsTo(Market::class,'marketold_id');
    }
    public function market()
    {
        return $this->belongsTo(Market::class,'market_id');
    }
    public function characteristics()
    {
        return $this->hasMany('App\Models\Characteristic');
    }

    public function multimedias()
    {
        return $this->hasMany('App\Models\Multimedia');
    }

    public function observations()
    {
        return $this->hasMany('App\Models\Observation');
    }

    public function financialInfo() {
        return $this->hasOne('App\Models\FinancialInfo');
    }

    public function binnacles()
    {
        return $this->hasMany('App\Models\Binnacle');
    }

    public function position()
    {
        return $this->hasMany('App\Models\Position','varity_id');
    }

    public function blocksvarity()
    {
        return $this->hasMany('App\Models\Blocksvarity','varity_id');
    }

    public function greenhouse()
    {
        return $this->belongsTo('App\GreenHouse','greenhouse_id');
    }
    
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
    }

    public function motherName()
    {
        if(!empty(@$this->mother_id)){
            if(@$this->mother->crossing == 0){
                return @$this->mother->name;
            }else{
                return @$this->mother->code;
            }
        }
        return "-";
    }

    public function fatherName()
    {
        if(!empty(@$this->father_id)){
            if(@$this->father->crossing == 0){
                return @$this->father->name;
            }else{
                return @$this->father->code;
            }
        }
        return "-";
    }

    public function avg_characteristics()
    {
        return $this->hasMany('App\Avg_characteristic');
    }

    public function traceability()
    {
        return $this->hasMany(Traceability::class);
    }

    public function relativeyieldvarities()
    {
        return $this->hasMany(Relativeyieldvarities::class);
    }

    public function getPositionSum(){

        $res = $this->position()->get()->sum( function($p){
            return $p->red + $p->blue + $p->yellow + $p->elongated; 
        });

        return $res;
    }

    public function getEfectivity(){

        $res = 0;
        $colors = $this->getPositionSum();

        if($colors > 0 && $this->glasses > 0 ){
            $res = round($colors*100/$this->glasses,2);
        }


        return $res;
    }
}
