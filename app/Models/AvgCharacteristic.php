<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvgCharacteristic extends Model
{	

	public function varity()
    {
        return $this->belongsTo(Varity::class,'varity_id');
    }

    
}
