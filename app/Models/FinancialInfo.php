<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancialInfo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'varity_id',
        'organization',
        'segment',
        'license_year',
        'region',
        'royalty_rate',
        'quantity_sold',
        'royalty_owed',
        'partnership',
        'seed_year',
        'product',
        'price_per_unit',
        'sales',
        'ware_price',
        'ware_sales',
        'date_of_reporting',
        'value_per_ha',
    ];

    public function varity()
    {
        return $this->belongsTo('App\Models\Varity');
    }
}
