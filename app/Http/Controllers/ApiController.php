<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Block;
use App\Models\Varity;
use App\Models\Blocksvarity;
use App\Models\Typefeature;
use App\Models\Characteristic;
use App\Models\Showfarm;
use App\Models\Feature;


class ApiController extends Controller
{
    public function export(){

    	$typefeature = Typefeature::all();
    	$feature = Feature::all();
    	$block = Block::where('offline',1)->get();
    	$blocksvarity = [];
    	$varities = [];
    	$characteristics = [];

    	foreach ($block as $key => $value) {
    		$b = Blocksvarity::where('block_id', $value->id)->get();

    		foreach ($b as $k => $v) {
    			$t = [
    				"id" => (int)$v->id,
    				'varity_id' => (int)$v->varity_id,
    				'block_id' => (int)$v->block_id,
    				'position' => (string)$v->position,
    				'type' => (string)$v->type,
    				'state' => 1,
    				'observation' => (string)$v->observation,
    				'last_position' => (int)$v->last_position,
    				'review' => (int)$v->review,
    				"created_at" => (string)$v->created_at,
    				"updated_at" => (string)$v->updated_at,
    			];

    			array_push($blocksvarity, $t);

    		}

    		$model = Blocksvarity::where('block_id', $value->id)->groupBy('varity_id')->distinct()->get();

    		foreach ($model as $k => $valores_) {
    		
    			$varity = $valores_->varity()->first();

    			if(!empty($varity)){
    				$valores = [
	    				"id" => (int)$varity->id,
	    				"mother_id" => (int)$varity->mother_id,
	    				"father_id" => (int)$varity->father_id,
	    				"code" => !empty($varity->code) ? (string)$varity->code : "-",
	    				"year" => (int)$varity->year,
	    				"name" => !empty($varity->name) ?(string)$varity->name: "-",
	    				"berries" => (int)$varity->berries,
	    				"crossing" => (int)$varity->crossing,
	    				"typecrossing" => (int)$varity->typecrossing,
	    				"observation" => empty($varity->observation) ?(string)$varity->observation: "-",
	    				"parent_id" => (int)$varity->parent_id,
	    				"user_id" => (int)$varity->user_id,
	    				"market_id" => (int)$varity->market_id,
	    				"slug" => empty($varity->slug) ?(string)$varity->slug : "-",
	    				"marketold_id" => !empty($varity->marketold_id) ? (int)$varity->marketold_id : (int)$varity->market_id,
	    				"mapout_id" => (int)$varity->mapout_id,
	    				"berries_objective" => (int)$varity->berries_objective,
	    				"status" => (int)$varity->status,
	    				"polem_save" => (int)$varity->polem_save,
	    				"glasses" => (int)$varity->glasses,
	    				"greenhouse_id" => (int)$varity->greenhouse_id,
	    				"created_at" => (string)$varity->created_at,
	    				"updated_at" => (string)$varity->updated_at
	    			];

	    			if(!$this->in_array_field("id",$varity->id, $varities)){
	    				array_push($varities, $valores);

	    				$char = $varity->characteristics()->get();

		    			if(!empty($char)){

		    				foreach ($char as $k_ => $v_) {
		    					$c = [
				    				"id" => (int)@$v_->id,
				    				"value" => (string)$v_->value,
				    				"feature_id" => (int)$v_->feature_id,
				    				"varity_id" => (int)$v_->varity_id,
				    				"year" => (int)$v_->year,
				    				"zonaltrial_id" => (int)$v_->zonaltrial_id,
				    				"block_id" => (int)$v_->block_id,
				    				"repeats_values" => (string)$v_->repeats_values,
				    				"created_at" => (string)$v_->created_at,
				    				"updated_at" => (string)$v_->updated_at,

				    			];

				    			array_push($characteristics, $c);
		    				}
		    			}
	    			}
    			}
    		}

    	}

    	$blocks = [];
    	$confBlock = [];

    	foreach ($block as $key => $value) {
    		$temp = [
    			"id" => $value->id,
    			"description" => $value->description,
    			"lines" => (int)$value->lines,
    			"limit" => (int)$value->limit,
    			"year" => (int)$value->year,
    			"latitud" => $value->latitud,
    			"longitud" => $value->longitud,
    			"offline" => (int)$value->ofline,
    			"created_at" => (string)$value->created_at,
    			"updated_at" => (string)$value->updated_at,
    		];	

    		array_push($blocks, $temp);

    		$showFarms = Showfarm::where('block_id', $value->id)->get();
    		if(!empty($showFarms)){
    			foreach ($showFarms as $key_ => $val_) {
    				$sf = [
    					"id" => (int)$val_->id,
    					"block_id" => (int)$val_->block_id,
    					"feature_id" => (int)$val_->feature_id,
    					"type" => (string)$val_->type,
    					"created_at" => (string)$value->created_at,
    					"updated_at" => (string)$value->updated_at,
    				];

    				array_push($confBlock, $sf);

    			}
    		}
    	}

    	return json_encode(["typefeature" =>  $typefeature, "feature" => $feature, "block" => $blocks, "blocksvarity" => $blocksvarity, "varities" => $varities, "char" => $characteristics, "showFarms" => $confBlock]);

    }

    public  function in_array_field($needle, $needle_field, $haystack, $strict = false) {
	    if ($strict) {
	        foreach ($haystack as $item)
	            if (isset($item->$needle_field) && $item->$needle_field === $needle)
	                return true;
	    }
	    else {
	        foreach ($haystack as $item)
	            if (isset($item->$needle_field) && $item->$needle_field == $needle)
	                return true;
	    }
	    return false;
	}

    public function import(Request $request){		

    }    			
}
