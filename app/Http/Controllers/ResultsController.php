<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Block;
use App\Models\Mapout;
use App\Models\Varity;
use App\Models\Market;
use App\Models\Blocksvarity;
use App\Models\Typefeature;
use App\Models\Characteristic;
use App\Models\Showfarm;
use App\Models\Feature;
use App\Models\Value;
use App\Locality;
use PDF;
use App\Avg_characteristic;



class ResultsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Vista de tipos de motor
     */
    public function index()
    {
        $markets = Market::pluck('name','id');
        $blocks_list  = Block::pluck('description','id');
        $years = Block::select('year')->distinct()->orderBy('year','desc')->pluck('year','year');
        $varities = Varity::select('name')->where('name', '!=', '')->get();
        $localities = Locality::pluck('name','id');

        #$blocks = Blocksvarity::all();

        return View('results.index',compact('blocks_list', 'markets', 'years','varities','localities'));
    }

    public function stadistics()
    {

        $valores = array("ADARA"=>array("57.2","55.4","58.2"),
                        "Asterix"=>array("48.9","54.3","52.5"),
                        "KAIA"=>array("56.5","57.0","61.0"),
                        "Karu"=>array("54.1","58.0","60.3"),
                        "Pukara"=>array("48.7","56.0","53.2"),
                        "TRAUKO"=>array("58.7","62.3","56.0")
                    );

        return View('stadistics.index',array("valores"=>$valores));
    }

    public function fries(){
        $markets = Market::pluck('name','id');
        $blocks_list  = Block::pluck('description','id');
        $years = Block::select('year')->distinct()->orderBy('year','desc')->pluck('year','year');
        $varities = Varity::select('name')->where('name', '!=', '')->get();

        return View('results.fries.index',compact('blocks_list', 'markets', 'years','varities'));
    }

    public function crossings(){
        return View('results.crossings.index');
    }

    public function getData(Request $request){

        $status = false;
        $res  = Blocksvarity::all();
        $data = [];
        $temp = [];
        $values = [];

        if(isset($request->block)){
            $res  = Blocksvarity::whereBlock_id($request->block)->get();
        }

        foreach ($res as $key => $value) {

            if($value->varity){

                $temp = array(
                    "code"   => @$value->varity->name,
                    "block"  => @$value->block->description,
                    "season" => $value->varity->year,
                    //"season_block" => $value->year,
                    //"father" => @$value->varity->father()->get()[0]->name,
                    //"mother" => @$value->varity->mother()->get()[0]->name,
                    //"market" => @$value->varity->market->name,
                    //'berries' => $value->varity->berries,
                    //'type' => $value->type,
                    //'block_id' => $value->id
                );

                foreach ($request->arr_caracteristicas as $k => $v) {


                    $avg = "-";
                    $avg_count = 0;

                    $c1 = Characteristic::where('varity_id',$value->varity->id)->where('feature_id',$v['IDFEATURE'])->where('year',0)->first();
                    $c1 = (!empty($c1)) ? $c1->value:null;
                    $c2 = Characteristic::where('varity_id',$value->varity->id)->where('feature_id',$v['IDFEATURE'])->where('year',1)->first();
                    $c2 = (!empty($c2)) ? $c2->value:null;

                    $c3 = Characteristic::where('varity_id',$value->varity->id)->where('feature_id',$v['IDFEATURE'])->where('year',2)->first();
                    $c3 = (!empty($c3)) ? $c3->value:null;
                    $c4 = Characteristic::where('varity_id',$value->varity->id)->where('feature_id',$v['IDFEATURE'])->where('year',3)->first();
                    $c4 = (!empty($c4)) ? $c4->value:null;
                    $c5 = Characteristic::where('varity_id',$value->varity->id)->where('feature_id',$v['IDFEATURE'])->where('year',4)->first();
                    $c5 = (!empty($c5)) ? $c5->value:null;
                    if($c1 != "") $avg_count++;
                    if($c2 != "") $avg_count++;
                    if($c3 != "") $avg_count++;
                    if($c4 != "") $avg_count++;
                    if($c5 != "") $avg_count++;

                    if(is_numeric($c1)){
                        $avg = $c1 + $c2 + $c3 + $c4 + $c5;
                        $avg = $avg/$avg_count;
                    }else{
                        if(is_null($c1)){
                            $c1 = "-";
                        }
                        $avg = $c1;
                    }


                    $temp += ["v".$v['IDFEATURE'] => $avg];

                }
                $temp += ["state"  => @$value->state];
                $temp += ["varity_id"  => @$value->varity->id];
                $temp += ["type"  => @$value->type];
                $temp += ["block_id"  => @$value->block_id];

                array_push($data, $temp);
            }
        }
        !empty($data) ? $status =  true: "";

        return json_encode(["data" => $data,"status" => $status]);
    }

    public function getProm($characteristic){

        return $prom;
    }

    public function getCaracteristicas(){

        $crossing = Varity::find($_GET['crossing']);
        $type = $_GET['type'];

        if(isset($_GET['unique'])){
            if($_GET['unique'] != ""){

                $f = Feature::find($_GET['unique']);
                $block = "";
                $typesfeatures = $f->typefeature;

                @$valor0 = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',0)->first()->value;
                @$valor1 = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',1)->first()->value;
                @$valor2 = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',2)->first()->value;
                @$valor3 = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',3)->first()->value;
                @$valor4 = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',4)->first()->value;


                return View('results.getCaracteristicas',compact('crossing','typesfeatures','block','type', 'f','valor0','valor1','valor2','valor3','valor4'));

            }
        }


        $block = Block::find($_GET['block_id']);
        $typesfeatures = Typefeature::All();

        foreach ($typesfeatures as $t_key => $t) {
            foreach ($t->features as $key => $f) {
               // if(Showfarm::where('block_id',$block->id)->count() == 0){
                #AÑO 0
                @$characteristic = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',0)->first();
                @$typesfeatures[$t_key]->features[$key]->valor0 = (!empty($characteristic)) ? $characteristic->value:null;
                @$typesfeatures[$t_key]->features[$key]->value_repeats_0 = (!empty($characteristic)) ? @$characteristic->repeats_values:null;
                #AÑO 1
                @$characteristic = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',1)->first();
                @$typesfeatures[$t_key]->features[$key]->valor1 = (!empty($characteristic)) ? $characteristic->value:null;
                @$typesfeatures[$t_key]->features[$key]->value_repeats_1 = (!empty($characteristic)) ? @$characteristic->repeats_values:null;
                #AÑO 2
                @$characteristic = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',2)->first();
                @$typesfeatures[$t_key]->features[$key]->valor2 = (!empty($characteristic)) ? $characteristic->value:null;
                @$typesfeatures[$t_key]->features[$key]->value_repeats_2 = (!empty($characteristic)) ? @$characteristic->repeats_values:null;
                #AÑO 3
                @$characteristic = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',3)->first();
                @$typesfeatures[$t_key]->features[$key]->valor3 = (!empty($characteristic)) ? $characteristic->value:null;
                @$typesfeatures[$t_key]->features[$key]->value_repeats_3 = (!empty($characteristic)) ? @$characteristic->repeats_values:null;
                #AÑO 4
                @$characteristic = Characteristic::where('varity_id',$crossing->id)->where('feature_id',$f->id)->where('year',4)->first();
                @$typesfeatures[$t_key]->features[$key]->valor4 = (!empty($characteristic)) ? $characteristic->value:null;
                @$typesfeatures[$t_key]->features[$key]->value_repeats_4 = (!empty($characteristic)) ? @$characteristic->repeats_values:null;
                /*}else{
                    unset($t->features[$key]);
                }*/
            }

        }

        return View('results.getCaracteristicas',compact('crossing','typesfeatures','block','type'));
    }

    public function modCaracteristicas(){

        $typesfeatures = Typefeature::All();


        return View('results.modCaracteristicas',compact('typesfeatures'));
    }

    public function preview_report(){
        $markets = Market::pluck('name','id');
        $blocks_list  = Block::pluck('description','id');

        return View('results.reports.preview_report',compact('markets','blocks_list'));
    }

    public function varities(Request $request){

        $block_data = Blocksvarity::whereBlock_id($request->block_id)->with('varity')->get();
        $block_data = $block_data->pluck('varity')->filter()->unique();

        return json_encode($block_data);
    }

    public function load_preview(Request $request){

        $data = [];

        foreach ($request->array_varity as $key => $value) {

            $varity = Varity::whereId($value['IDVARITY'])->first();
            $typefeaturearray=Array();
            $typefselector=Array();
            if($varity){

                $temp = array(
                    "id" =>  $varity->id,
                    "code"   => @$varity->name,
                    "season" => $varity->year,
                    "resultados" => [],
                    "multimedia" => $varity->multimedias
                );


                foreach ($request->array_find as $k => $v) {


                    $avg = "-";
                    $std = 0;
                    $resultado="";

                    $typefname = Feature::find($v['IDFEATURE']);
                    $char = Avg_characteristic::whereVarity_id($varity->id)->whereFeature_id($v['IDFEATURE'])->first();

                    if(count($temp['resultados']) == 0 ){
                        $first_char = $typefname->typefeature->name;
                    }else{
                        $first_char = "";
                    }

                    $temp['resultados'][] = [ "PROM" => @$char->PROMEDIO? $char->PROMEDIO : 0,
                                              "STD" => @$char->STD ? $char->STD : "-",
                                              'NAME' => @$typefname->name? $typefname->name : 0,
                                              "VAL" => "",
                                              "TYPE" => $first_char];

                    array_push($typefeaturearray,$typefname->typefeature->name);
                }

                array_push($data, $temp);
            }
        }
        return View('results.reports.preview_pdf', compact('data','typefeaturearray'));
    }

    function stats_standard_deviation(array $a, $sample = false) {
        $n = count($a);
        if ($n === 0) {
            trigger_error("The array has zero elements", E_USER_WARNING);
            return false;
        }
        if ($sample && $n === 1) {
            trigger_error("The array has only 1 element", E_USER_WARNING);
            return false;
        }
        $mean = array_sum($a) / $n;
        $carry = 0.0;
        foreach ($a as $val) {
            $d = ((double) $val) - $mean;
            $carry += $d * $d;
        };
        if ($sample) {
           --$n;
        }
        return sqrt($carry / $n);
    }

    public function report(Request $request){
        $data = json_decode($_POST["data"]);

        $pdf = PDF::loadView('results.reports.report', compact('data'));
        //Aqui guardo el reporte.
        //Storage::put($sample->report_route, $pdf->output());

        //Descargo el reporte
        return $pdf->stream('pdf');
    }

    public function info_to_compare(Request $request){
        $data    = [];
        $headers = [];
        $resultado = [];

        #$f = Feature::OrderBy('typefeature_id')->OrderBy('name', 'asc')->pluck('name','id')->toArray();
        $f = Feature::OrderBy('name', 'asc')->get();

        foreach ($f as $key => $value) {
            if(!isset($data[$value->id])){
                $data[$value->id] = array(
                    "NAME" => $value
                );
            }
        }


        foreach ($request->selected_p as $k => $v) {
            foreach ($data as $key => $value) {
                $std = 0;
                $c = Characteristic::where('varity_id', $v["IDVARITY"])->where('feature_id', $key)->where('value', 'not like', '0')->get();
                $res = "-";

                if($c->count() > 0){
                    if($c[0]->value > 0){
                        $res = round(@$c->avg('value'), 2);
                        $std = @$this->stats_standard_deviation_model($c);
                    }else{
                        $res = $c[0]->value;
                    }
                }

                if(!isset($data[$v["IDVARITY"]])){
                    $data[$key][$v["IDVARITY"]] = array(
                        "value" => $res,
                        "std" => $std,
                        "num" => count($c)
                    );
                }
            }
        }


        return json_encode(["headers" => $headers, "data" => $data]);
    }

    public function getVaritiesFromBlock(Request $request){
        $varities = [];
        $position = Blocksvarity::where('block_id', $request->block)->whereNotNull('varity_id')->distinct('varity_id')->get();
        foreach ($position as $key => $value) {
            $name = @$value->varity->name;
            if(!empty($name)){
                array_push($varities, ["id" => $value->varity_id,"name" => $name]);
            }
        }

        return json_encode($varities);
    }

    public function getChildrensFromVarity(Request $request){
        $childs = [];
        $childrens = Varity::Where('father_id', $request->varity)->orWhere('mother_id', $request->varity)->get();

        foreach ($childrens as $key => $value) {
            $name = $value->name;

            if(!empty($name)){
                array_push($childs, ["id" => $value->id,"name" => $name, "father_name" => @$value->father->name, "mother_name" => @$value->mother->name]);

            }
        }
        return json_encode($childs);
    }

    public function getBlocksByYear(Request $request){

        $model = Block::select('id', 'description')->where('year',$request->year);

        if(isset($request->locality) && $request->locality != ""){
            $model = $model->where("locality", $request->locality);
        }

        $blocks = $model->get();

        #disernir los repetidos
        foreach ($blocks as $key => $value) {
            $count = Blocksvarity::where('block_id', $value->id)->where('varity_id','>',0)->count();
            $value->description .= "(".$count.")";
        }

        return json_encode($blocks);
    }

    public function getVarityByname(Request $request){

        $result = Varity::where('name','like',$request->year)->first();
        $varity = [];
        if(!empty($result)){
             $varity = [
                "IDVARITY" => $result->id,
                "NAME" => $result->name.';'.@$result->father->name.';'.$result->mother->name
             ];
        }

        return json_encode($varity);
    }

    public function getVarityBynameInfo(Request $request){

        $varity = Varity::where('name','like',$request->name)->first();
        $typesfeatures = Typefeature::where('status', 1)->get();

        foreach ($typesfeatures as $t_key => $t) {
            $m = $t->features()->where('status',1)->orderBy('id', 'asc')->get();

            foreach ($m as $key => $f) {

                $c = Characteristic::select('value')->where('varity_id',$varity->id)->where('feature_id',$f->id)->where('value','!=', '')->get();

                if(!empty($c) && $c->count() > 0){
                    if(is_numeric($c[0]->value)){

                        if($f->id == 16 || $f->id == 18){
                            #$typesfeatures[$t_key]->features[$key]->promedio = $c[0]->value;
                            foreach ($typesfeatures[$t_key]->features as $key_ => $val) {
                                if($val->id == $f->id){
                                    $typesfeatures[$t_key]->features[$key_]->promedio = $c[0]->value;
                                    #$typesfeatures[$t_key]->features[$key_]->std = $this->stats_standard_deviation($c);
                                }
                            }
                        }else{
                            foreach ($typesfeatures[$t_key]->features as $key_ => $val) {
                                if($val->id == $f->id){
                                    $typesfeatures[$t_key]->features[$key_]->promedio = (int)$c->avg('value');
                                    #$typesfeatures[$t_key]->features[$key_]->std = $this->stats_standard_deviation($c);
                                }
                            }
                        }

                    }else{
                        $typesfeatures[$t_key]->features[$key]->promedio = @$c->first()->value;
                        #$typesfeatures[$t_key]->features[$key]->std = "-";
                    }
                }
            }
        }

        $childrens = Varity::whereNotNull('name')->where('mother_id',$varity->id)->orWhere('father_id',$varity->id)->get();
        $tree = Varity::whereIn('mother_id',[$varity->father_id, $varity->mother_id])->whereIn('father_id',[$varity->father_id, $varity->mother_id])->get();

        $count_childrens = count($childrens);
        $nietos = 0;
        if($count_childrens > 0){
            foreach ($childrens as $key => $value) {
                $count = Varity::whereNotNull('name')->where('mother_id',$value->id)->orWhere('father_id',$value->id)->count();
                $nietos += $count;
            }
        }

        $blocks = Blocksvarity::where('varity_id', $varity->id)->get();

        return View('results.varityResume',compact('varity','typesfeatures','tree', 'ancesters', 'childrens', 'blocks'));
    }

    function stats_standard_deviation_model( $a, $sample = false) {
        $n = count($a);
        if ($n === 0) {
            trigger_error("The array has zero elements", E_USER_WARNING);
            return false;
        }
        if ($sample && $n === 1) {
            trigger_error("The array has only 1 element", E_USER_WARNING);
            return false;
        }

        $mean = $a->sum('value') / $n;

        $carry = 0.0;

        foreach ($a as $val) {
            $d = ((double) $val->value) - $mean;
            $carry += $d * $d;
        };
        if ($sample) {
           --$n;
        }
        return round(sqrt($carry / $n), 2);
    }

    public function getDataFeature(Request $request){
        $r = Characteristic::where('feature_id', $request->feature)->where('varity_id', $request->varity)->get();

        foreach ($r as $key => $value) {
            $response[$value['year']][] = $value;
        }

        return View('results.dataFeatures',compact('response'));
    }




}
