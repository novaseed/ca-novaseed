<?php

namespace App\Http\Controllers;

use App\Models\Value;
use App\Models\Feature;
use App\Models\Typefeature;
use Illuminate\Http\Request;
use Illuminate\Support\Str as Str;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $typefeatures = Typefeature::All();
        return View('features.index',compact('typefeatures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('features.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['slug' => Str::slug($request->input('name'))]);

        $feature = Feature::create($request->all());

        if($feature){
            //recorremos los valores de la característica
            if(!empty($request->input('values'))){
                foreach ($request->input('values') as $v) {
                    $value = new Value();
                    $value->name = trim($v['name']);
                    $value->value = $v['value'];
                    $value->feature_id = $feature->id;
                    $value->save();
                }
            }
        }

        return redirect()->route('features.index')
                        ->with('success','Feature saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function show(Feature $feature)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function edit(Feature $feature)
    {
        return view('features.edit',compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feature $feature)
    {
        if($feature->update($request->all())){
            //Value::where('feature_id',$feature->id)->delete();
            //recorremos los valores de la característica
            if(!empty($request->input('values'))){
                foreach ($request->input('values') as $v) {
                    if(!empty($v['id'])){
                        $value = Value::find($v['id']);
                    }else{
                        $value = new Value();
                    }
                    $value->name = $v['name'];
                    $value->value = $v['value'];
                    $value->feature_id = $feature->id;
                    $value->save();
                }
            }    
        }

        

        return redirect()->route('features.index')
                        ->with('success','Feature saved successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feature $feature)
    {
        $feature->delete();

        return redirect()->route('features.index')
                        ->with('success','Feature deleted successfully.');
    }

}
