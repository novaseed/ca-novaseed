<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\VaritiesImport;
use App\Colors;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $colors = Colors::all();
       
        return view('home', compact('colors'));
    }

    /**
     * Función para cargar el excel 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function import()
    {
        return view('imports.import');
    }

    /**
     * Función para cargar el excel 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function importExcel()
    {
        Excel::import(new VaritiesImport, request()->file('file'));
    }

    
}
