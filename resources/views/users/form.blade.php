<?php $roles = App\Role::pluck('description','id');?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Ingrese Nombre','class' => 'form-control required')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <strong>Username:</strong>
            {!! Form::text('rut', null, array('placeholder' => 'Enter Username','class' => 'form-control required rut')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <strong>Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Enter Email','class' => 'form-control email')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <strong>Telephone:</strong>
            {!! Form::text('phone', null, array('placeholder' => 'Enter Telephone','class' => 'form-control number')) !!}
        </div>
    </div>
    
    <hr style="border-top: 2px solid #ececec;    width: 97%;    margin-bottom: 25px;margin-top: 25px;">
    
    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
            <strong>Password:</strong>
            <input name="password" type="password" class="form-control required" name="password"  placeholder="Enter Password">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
            <strong>Re-Enter Password:</strong>
            <input name="password_repeat" type="password" class="form-control required" name="password"  placeholder="Enter Password">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
            <strong>User Type:</strong>
            {!! Form::select('role_id', $roles,null,['class' => 'form-control required','placeholder' => 'Select User Type']); !!}
        </div>
    </div>

</div>
    <!--
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Body:</strong>
            {!! Form::textarea('body', null, array('placeholder' => 'Body','class' => 'form-control','style'=>'height:150px')) !!}
        </div>
    </div>-->

<script>
    $(document).ready(function(){
        $("select[name=enterprise_id]").change(function(){
            if($(this).val() != ""){
                $("select[name=location_id]").removeAttr("disabled")
                $.ajax({
                    url: '/locations/json/'+$("select[name=enterprise_id]").val(),
                    type: 'GET',
                    dataType: 'json',
                }).done(function(data) {
                    $("select[name=location_id]").html('<option value="" selected="selected">Select Location</option>')
                    if(data != null){
                        $.each(data,function(i,location){
                            $("select[name=location_id]").append('<option value="'+location.id+'">'+location.name+'</option>')
                        })
                    }
                })
            }else{
                $("select[name=location_id]").attr("disabled","disabled")
            }
        })

        $("select[name=role_id]").change(function(){
            $("select[name=location_id]").parent().parent().hide()
            $("select[name=enterprise_id]").parent().parent().hide()
            if($(this).val() == 2){
                $("select[name=enterprise_id]").parent().parent().show()
            }else if($(this).val() == 3){
                $("select[name=location_id]").parent().parent().show()
                $("select[name=enterprise_id]").parent().parent().show()
            }
        })

        
    })
</script>
