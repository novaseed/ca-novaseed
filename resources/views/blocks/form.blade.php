<?php $lineas = array(1 => '1 linea',2 => '2 lineas',3 => '3 lineas', 4 => '4 lineas', 5 => '5 lineas',6 => '6 lineas',7 => '7 lineas',8 => '8 lineas',9 => '9 lineas',10 => '10 lineas',11 => '11 lineas',12 => '12 lineas');

    $locality = array(1 => "Pto. Varas", 2 => "Pta. Arenas");
?>
<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Description:</strong>
            {!! Form::text('description', null, array('placeholder' => 'Enter description','class' => 'form-control required ')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Year:</strong>
            {!! Form::text('year', null, array('placeholder' => 'Enter year','class' => 'form-control required')) !!}
        </div>
    </div>


    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Number of Lines:</strong>
            {!! Form::text('lines', null, array('placeholder' => 'Enter amount','class' => 'form-control required')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4"> 
        <div class="form-group">
            <strong>Location:</strong>
            {!! Form::select('locality', $locality,null,['class' => 'form-control required','placeholder' => 'Select Location']) !!}
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <strong>Latitude:</strong>
            {!! Form::text('latitude', null, array('placeholder' => 'Select a point on the map','class' => 'form-control required')) !!}
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <strong>Longitude:</strong>
            {!! Form::text('longitude', null, array('placeholder' => 'Select a point on the map','class' => 'form-control required')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div id="google_maps">
            <input id="pac-input" class="controls" type="text" placeholder="Write address here">
            <div id="map" style="height: 400px;width: 100%;margin-bottom: 30px;"></div>
        </div>
    </div>
</div>

