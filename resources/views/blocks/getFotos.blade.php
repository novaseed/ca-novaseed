<div class="row">
    <div class="col-lg-4">
        <fieldset>
            <legend>Add Photo</legend>
            {!! Form::open(array('class' => 'm-form','url' => '/crossings/uploadPhoto','method'=>'POST','id' => 'FormAddPhoto','files' => true)) !!}
                <input type="hidden" name="crossing" value="<?= $crossing->id;?>">
                <div class="form-group">
                    {!! Form::select('year', array(0 => 'Year 0',1 => 'Year 1',2 => 'Year 2',3 => 'Year 3',4 => 'Year 4') ,null,['class' => 'form-control required']); !!}
                </div>    
                <div class="form-group">
                    <strong>Archive:</strong><br>
                    {!! Form::file('file',  array('placeholder' => 'Add Archive','class' => ' required')) !!}
                </div>
                <button class="btn btn-success"><i class="fa fa-save"></i> Upload Photo</button>
            {!! Form::close() !!}
        </fieldset>   
    </div>
    <div class="col-lg-8">
        <fieldset>
            <legend>List of Photos</legend>
            @include('crossings.show.listPhotos',['multimedias' => $crossing->multimedias])
        </fieldset>
    </div>
</div>