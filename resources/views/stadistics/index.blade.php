@extends('layouts.app')
@section('content')
<div class="page-title">
	<i class="fa fa-file"></i>
	<h3><span class="semi-bold">Random Complete Blocks</span></h3>
</div>
<div class="row">
    <div class="col-md-12">
		<div class="grid simple">
			<div class="grid-body">
                <table class="table" id="tableFacturacion">
						<thead>
					    	<tr>
                                <th></th>
								<th>R1</th>
                                <th>R2</th>
                                <th>R3</th>
							</tr>
						</thead>
						<tbody>

                            <?php foreach ($valores as $key => $valor):?>
                                <tr>
                                    <th><?php echo $key?></th>
                                    <td><?php echo $valor[0]?></td>
                                    <td><?php echo $valor[1]?></td>
                                    <td><?php echo $valor[2]?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
					</table>
                    <div class="col-md-12">
                        <div class="col-md-2">
							<button class="btn btn-block btn-warning" type="button" style="margin-top: 28px;" id="calc">
								<i class="fa fa-search"></i> Calculate
							</button>
					    </div>
					</div>

				<hr>
                <div id="contenedor">


				</div>
			</div>
		</div>
	</div>
</div>
<script>

    $(document).ready(function(){

    arr_rep=[];

    $("#calc").click(function(){
        cv_ensayo();
        cv_repeticion();
        resumen();
        analisis();

	})

    cv_ensayo = function(){
        suma=0;
        cont=0;
        varianza=0;
        document.querySelectorAll('#tableFacturacion tbody tr').forEach(function(e){
            var columnas=e.querySelectorAll("td");

            for(i=0;i<=2;i++){
                cont++;
                suma+=parseFloat(columnas[i].textContent);
            }

        });
        promedio = (suma/cont);
        document.querySelectorAll('#tableFacturacion tbody tr').forEach(function(e){
            var columnas=e.querySelectorAll("td");

            for(i=0;i<=2;i++){

                rango = Math.pow(parseFloat(columnas[i].textContent)-promedio,2);
                varianza+=rango;
            }

        });

        desviacion = Math.sqrt(varianza/cont);

        c_variacion = Math.round((desviacion/promedio)*100);

    }

    cv_repeticion = function(){
        arr_rep=[];

        for(i=0;i<=2;i++){
            suma=0;
            cont=0;
            varianza=0;
            document.querySelectorAll('#tableFacturacion tbody tr').forEach(function(e){
                var columnas=e.querySelectorAll("td");

                cont++;
                suma+=parseFloat(columnas[i].textContent);

            });
            promedio = (suma/cont);
            document.querySelectorAll('#tableFacturacion tbody tr').forEach(function(e){
                var columnas=e.querySelectorAll("td");

                rango = Math.pow(parseFloat(columnas[i].textContent)-promedio,2);
                varianza+=rango;

            });

            desviacion = Math.sqrt(varianza/cont);

            c_variacion = Math.round((desviacion/promedio)*100);

            promedio=Math.round(promedio);
            desviacion=Math.round(desviacion);
            let fila={
                promedio:promedio,
                desviacion:desviacion,
                c_variacion: c_variacion

            };
            arr_rep.push(fila);

        }
        console.log(arr_rep)
    }

    resumen = function(){
        arr_resumen=[];
        suma=0;
        varianza=0;
        document.querySelectorAll('#tableFacturacion tbody tr').forEach(function(e){
            var columnas=e.querySelectorAll("td");
            for(i=0;i<=2;i++){
                suma+=parseFloat(columnas[i].textContent);
            }
            promedio = roundToTwo(suma/3);
            for(i=0;i<=2;i++){
                rango = Math.pow(parseFloat(columnas[i].textContent)-promedio,2);
                varianza+=rango;
            }
            let fila={
                suma:roundToTwo(suma),
                promedio:promedio,
                varianza: roundToTwo(varianza/2)

            };
            arr_resumen.push(fila);
            varianza=0;
            suma=0;
        });
        console.log(arr_resumen)
    }
    analisis = function(){
        suma=0;
        cont=0;
        suma_pow=0;
        sTC=0;
        sCD=0;

        document.querySelectorAll('#tableFacturacion tbody tr').forEach(function(e){
            var columnas=e.querySelectorAll("td");

            for(i=0;i<=2;i++){
                cont++;
                suma+=parseFloat(columnas[i].textContent);
                suma_pow+=(Math.pow(parseFloat(columnas[i].textContent),2))/5;
            }

        });

        prom=suma/cont;

        document.querySelectorAll('#tableFacturacion tbody tr').forEach(function(e){
            var columnas=e.querySelectorAll("td");
                for(i=0;i<=2;i++){
                    rango = Math.pow(parseFloat(columnas[i].textContent)-prom,2);
                    sTC+=rango;
                    sCD+= Math.pow(parseFloat(columnas[i].textContent)-arr_rep[i]["promedio"],2);
                }
        });



        fact_c = Math.pow(suma,2)/cont;

        sctotales = suma_pow-fact_c;
        console.log(sTC)

    }

    function roundToTwo(num) {
        return +(Math.round(num + "e+3")  + "e-3");
    }


})
</script>

@endsection
