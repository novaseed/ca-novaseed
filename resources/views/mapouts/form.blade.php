<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Description:</strong>
            {!! Form::text('description', null, array('placeholder' => 'T2018-2019','class' => 'form-control  required')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Year:</strong>
            {!! Form::text('year', null, array('placeholder' => '2020','class' => 'form-control  required numeric')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Line:</strong>
            {!! Form::text('lines', null, array('placeholder' => 'Enter line number','class' => 'form-control  required')) !!}
        </div>
    </div>
</div>