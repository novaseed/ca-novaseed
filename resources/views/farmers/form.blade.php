<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>Name:</label>
            {!! Form::text('name', null, array('placeholder' => 'Enter Name','class' => 'form-control required')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>ID:</label>
            {!! Form::text('rut', null, array('placeholder' => 'Enter ID','class' => 'form-control required rut')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>Email:</label>
            {!! Form::text('email', null, array('placeholder' => 'Enter Email','class' => 'form-control email')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>Telephone:</label>
            {!! Form::text('phone', null, array('placeholder' => 'Enter Telephone','class' => 'form-control number')) !!}
        </div>
    </div>
    
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <label>Description:</label>
            {!! Form::text('description', null, array('placeholder' => 'Enter Description','class' => 'form-control ')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <label for="name">Region</label>
        <select class="form-control required" name="region_id">
            <option value="">Select Region</option>
            @foreach($region as $v)
                <option value="{{$v->id}}">{{$v->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-4">
        {!!Form::label('Province')!!}
        {!!Form::select('province_id',array(), null,['class' => 'form-control required select2 provinces','placeholder' => 'Select Province'])!!}
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        {!!Form::label('Community')!!}
        {!!Form::select('community_id',array(), null,['class' => 'form-control required select2','placeholder' => 'Select Community'])!!}
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <label>Latitude:</label>
            {!! Form::text('latitude', null, array('placeholder' => 'Select a point on the map','class' => 'form-control', 'readonly' => 'readonly')) !!}
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <label>Longitude:</label>
            {!! Form::text('longitude', null, array('placeholder' => 'Select a point on the map','class' => 'form-control', 'readonly' => 'readonly')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div id="google_maps">
            <input id="pac-input" class="controls" type="text" placeholder="Enter address">
            <div id="map" style="height: 400px;width: 100%;margin-bottom: 30px;"></div>
        </div>
    </div>
</div>
    <!--
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Body:</strong>
            {!! Form::textarea('body', null, array('placeholder' => 'Body','class' => 'form-control','style'=>'height:150px')) !!}
        </div>
    </div>-->

<script>
    $(document).ready(function(){
        $("select[name=region_id]").change(function(){
            region_id = $(this).val()
            if(region_id != ""){
                $("select[name=province_id]").html('<option value="">Select Province</option>')
                $("select[name=community_id]").html('<option value="">Select Community</option>')
                $.getJSON("/farmers/provinces?region_id="+region_id, function( data ) {
                    var items = [];
                    $.each( data, function( key, val ) {
                        $("select[name=province_id]").append( "<option value='" + val.id + "'>" + val.name + "</option>" );
                    });
                    $("select[name=province_id]").removeAttr("disabled")
                    
                });
            }
        })

        $("select[name=province_id]").change(function(){
            province_id = $(this).val()
            if(province_id != ""){
                $("select[name=community_id]").html('<option value="">Select City</option>')
                $.getJSON( "/farmers/communities?province_id="+province_id, function( data ) {
                    var items = [];
                    $.each( data, function( key, val ) {
                        $("select[name=community_id]").append( "<option value='" + val.id + "'>" + val.name + "</option>" );
                        $("select[name=community_id_dynamic]").append( "<option value='" + val.id + "'>" + val.name + "</option>" );
                    });
                    $("select[name=community_id]").removeAttr("disabled")
                });
            }
        })

        $("select[name=community_id]").change(function(){
            var text = $("select[name=community_id] option:selected").text()
            $("#pac-input").val(text).change()
        })
    })
</script>
