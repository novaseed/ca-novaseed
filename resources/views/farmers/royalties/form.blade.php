<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>Year:</label>
            {!! Form::text('year', null, array('placeholder' => 'Enter Year','class' => 'form-control required')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>Season:</label>
            {!! Form::text('season', null, array('placeholder' => 'Enter Season','class' => 'form-control ')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>Amount:</label>
            {!! Form::text('pay', null, array('placeholder' => 'Enter Amount','class' => 'form-control required')) !!}
        </div>
    </div>

    {!! Form::hidden('farmer_id', $farmer_id, array('placeholder' => 'Enter Amount','class' => 'form-control')) !!}
   
</div>


<script>
    $(document).ready(function(){
    })
</script>
