<div class="row">
    

    <div class="col-xs-6 col-sm-6 col-md-3">
        <label for="name">Farmer</label>
        {!! Form::select('farmer_id', $farmer,null,['class' => 'form-control required','placeholder' => 'Select Farmer']); !!}
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3">
        {!!Form::label('Field')!!}
        {!!Form::select('land_id',array(), null,['class' => 'form-control required select2','placeholder' => 'Select Field'])!!}
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3">
        <label for="name">Variety</label>
        {!! Form::select('varity_id', $varity,null,['class' => 'form-control required','placeholder' => 'Select Variety']); !!}

    </div>

    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="form-group">
            <label>Quantity Tonne(ha):</label>
            {!! Form::text('quantity', null, array('placeholder' => 'Enter Quantity','class' => 'form-control required')) !!}
        </div>
    </div>

    <div class="col-lg-4">
        <div class="form-group">
            <label>Production:</label>
            {!! Form::text('production', null, array('placeholder' => 'Enter Production','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <label>Category:</label>
            {!! Form::select('certifies_id', $certifies,null,['class' => 'form-control required','placeholder' => 'Select Category']); !!}
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <label>Supplier:</label>
            {!! Form::text('provider', "Novaseed", array('placeholder' => 'Enter Supplier','class' => 'form-control')) !!}
        </div>
    </div>
</div>
    <!--
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Body:</strong>
            {!! Form::textarea('body', null, array('placeholder' => 'Body','class' => 'form-control','style'=>'height:150px')) !!}
        </div>
    </div>-->

<script>
    $(document).ready(function(){
        $("select[name=farmer_id]").change(function(){
            farmer_id = $(this).val()
            if(farmer_id != ""){
                $("select[name=land_id]").html('<option value="">Select Field</option>')
                $.getJSON("/farmers/lands?farmer_id="+farmer_id, function( data ) {
                    var items = [];
                    $.each( data, function( key, val ) {
                        $("select[name=land_id]").append( "<option value='" + val.id + "'>" + val.name + "</option>" );
                    });
                    $("select[name=land_id]").removeAttr("disabled")
                });
            }
        })
    })
</script>
