@extends('layouts.app')
@section('content')



<div class="m-portlet m-portlet--head-solid-bg m-portlet--rounded">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                    <i class="flaticon-calendar"></i>
                </span>

                <h3 class="m-portlet__head-text" >
                    Prospect Finder
                </h3>
            </div>          
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <button type="button" id="btn-agregar-caracteristica" class="btn btn-outline-success">
                        Add Characteristics
                    </button>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <form id="search-varity">
            
        </form>
        <button type="button" id="buscar-prospecto" class="btn btn-success" >Find</button>
        <div id="resultado">
            
        </div>
    </div>
</div>

<div class="modal fade show" id="modal-colors" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" role="document" style="min-width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Información Colores</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    @foreach ($colors as $v)
                        <li>
                            {{$v->name}} <i class="fa fa-circle" style="color: {{$v->hexa}}"></i> | valor: {{$v->value}} 
                        </li>
                    @endforeach
                </ul>
                <h5>
                For value mapping, this corresponds to the mix between the predominant color and a secondary color
                </h5>
                <h5>
                For example, if brown predominates but also white, the value to consult would be <strong>31</strong>
                </h5>
            </div>
        </div>
    </div>
</div>

<div class="m-portlet m-portlet--head-solid-bg m-portlet--rounded">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-calendar"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Version 0.1 (last update: 2021-08-13) 
                </h3>
            </div>          
        </div>
        <div class="m-portlet__head-tools">
            <!--<ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <button type="button" id="btn-agregar-caracteristica" class="btn btn-outline-success">
                        Agregar Característica
                    </button>
                </li>
            </ul>-->
        </div>
    </div>
    <div class="m-portlet__body">
        <p>
            Modules Made:
            <ul>
                <li>Added atribute sex for Varities</li>
                <li>Added variety type to Create or Edit Varieties</li>
                <li>Translate Variety Module</li>
                <li>Added New Families Characteristics</li>
            </ul>
            Pending Update
             <ul>
                <li>Translate Rest of modules</li>
                <li>Add new Features</li>
                
            </ul>
            <hr>
        </p>
    </div>
</div>

<script>
    $(document).ready(function(){



        $(".timepicker").datepicker({
        });

        var i = 0;
        $("#btn-agregar-caracteristica").click(function(){
            $.ajax({
                url: '/varities/addFeature',
                type: 'get',
                dataType: 'html',
                data:{ i: i}
            }).done(function(data) {
                i = i + 1
                $("#search-varity").append(data)
            })
        })

        getDataFeature = function(val){
            if(val.value != ""){
                $.ajax({
                    url: '/varities/addValues',
                    type: 'get',
                    dataType: 'html',
                    data:{ feature: val.value}
                }).done(function(data) {
                    $(val).parent().next('.col-lg-8').html(data)
                })
            }
        }

        $("#buscar-prospecto").click(function(){
            $.ajax({
                url: '/varities/searchHome',
                type: 'get',
                dataType: 'html',
                data: $("#search-varity").serialize()
            }).done(function(data) {
                console.log(data)
                if(data){
                   $("#resultado").html(data)
                }else{
                    alert("No data Found")
                }
                
            })
        })

        eliminarValor = function(element){
            $(element).parent().parent().parent().remove()
        }

        

    })
</script>
@endsection
