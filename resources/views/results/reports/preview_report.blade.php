@extends('layouts.app')
@section('content')
<div class="m-portlet m-portlet--head-solid-bg m-portlet--rounded">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-list"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Report Preview
                </h3>
            </div>          
        </div>
    </div>
    <div class="m-portlet__body">
    	<div class="row" style="margin-bottom: 20px;">
            <div class="col-lg-3">
                <label for="">Trials</label>
                {!! Form::select('block_id', $blocks_list,null,['class' => 'form-control required','placeholder' => 'No Filters', 'id'=>'block_id']); !!}
            </div>
            <div class="col-lg-5">
                <button type="button" id="buscar" class="btn btn-primary" style="margin-top: 23px;float: right;" onclick="loadData()"><i class="fa fa-search"></i> Select</button>
                <button type="button" id="buscar" class="btn btn-primary" style="margin-top: 23px;float: right; margin-right: 10px;" onclick="modificarCaracteristicas()"><i class="fa fa-check"></i> Characteristics</button>
                <button type="button" id="buscar" class="btn btn-primary" style="margin-top: 23px;float: right; margin-right: 10px;" onclick="seleccionarVariedades()"><i class="fa fa-check"></i> Varieties</button>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet m-portlet--head-solid-bg m-portlet--rounded">
    <div class="m-portlet__body" id="preview_container">
    	
    </div>
</div>

<div class="modal fade show" id="modal-caracteristicas" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" role="document" style="min-width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Characteristics</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div class="modal fade show" id="modal-varities" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" role="document" style="min-width: 1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Varieties</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-lg-12">
	       				<table class="table-striped- table-bordered table-hover table-checkable datatables_p" id="table_varities">
	       					<thead>
				                <tr>
				                    <th>Variety</th>
				                    <th style="width:400px"></th>
				                </tr>
	            			</thead>
	       				</table>
	            	</div>
	            	<button type="button" class="btn btn-success" style="float:right" data-dismiss="modal" aria-label="Close"><i class="fa fa-save"></i> Continue</button>
            	</div>
            	
            	
            </div>
        </div>
    </div>
</div>
<script>

	array_varity = [];

    $(document).ready(function(){

    	table_varities = $("#table_varities").DataTable({
    		bPaginate :false,
    		scrollY: "300px",
    	});

    	modificarCaracteristicas = function(id = null){	
	        $.ajax({
	            url: '/results/modCaracteristicas',
	            type: 'GET',
	            dataType: 'html',
	            data:{id:id}
	        }).done(function(data) {
	            array_find = [];
	            $("#modal-caracteristicas .modal-body").html(data)
	            $("#modal-caracteristicas").modal("show")
	        })
	        
	    }

    	selectTypes = function(index, id){
	        if($(index).prop('checked')){
                //$(".type_"+id).attr('checked','true')
	            $(".type_"+id).click()
	        }else{
	            $(".type_"+id).removeAttr('checked')
	        }
	    }

    	registrarCheck = function(indice,name){
	        var item = {};
	        
	        for (var i = 0; i < array_find.length; i++) {
	            if ( (array_find[i]['IDFEATURE'] == indice)) {
	                array_find.splice(i, 1);
	            };
	        };

	        if ( $("#index_"+indice).is(':checked') ) {
	            item['IDFEATURE'] = indice;
	            item['NAMEFEATURE'] = name;
	            array_find.push(item);
	        }
	    }

	    registrarVarities = function(indice,id_varity){
	        var item = {};
	        
	        for (var i = 0; i < array_varity.length; i++) {
	            if ( (array_varity[i]['INDICE'] == indice)) {
	                array_find.splice(i, 1);
	            };
	        };

	        if ( $("#check_"+indice).is(':checked') ) {
	            item['INDICE'] = indice;
	            item['IDVARITY'] = id_varity;
	            array_varity.push(item);
	        }
	    }

        seleccionarVariedades = function(){
        	$.ajax({
                url: '/results/getVarities',
                type: 'GET',
                dataType: 'json',
                data: {block_id: $("#block_id").val()},
            }).done(function(data) {
                table_varities.clear().draw();
                $.each(data, function(i,v){
                	console.log(v)
                	if(typeof v.id !== 'undefined"'){
                		var btn = '<input  type="checkbox" id="check_'+i+'" onclick="registrarVarities('+i+','+v.id+')">'
	                	table_varities.row.add([
	                		v.name == " "? v.code : v.name,
	                		btn
	                	]).draw();
                	}
                })
                array_varity = [];
	            $("#modal-varities").modal("show")

            })
        }

        loadData = function(){
        	$.ajax({
	            url: '/results/load_preview',
	            type: 'GET',
	            dataType: 'html',
	            data:{array_find:array_find,array_varity: array_varity}
	        }).done(function(data) {
	            $("#preview_container").html(data)
	        })
        }
    })
</script>
@endsection
