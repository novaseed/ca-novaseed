<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Enter Feature','class' => 'form-control  required')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Feature Type:</strong>
            {!! Form::select('typefeature_id', [19 => 'Seed', 20 => 'Tuber Uniformity', 21 => 'Plant Uniformity', 22 => 'Plant Characteristics', 23 => 'Stem Characteristics', 24 => 'Leaf Characteristics', 25 => 'Inflorescence', 26 => 'Tubers', 27 => 'Sprouts', 28 => 'Agricultural Characteristics', 29 => 'Culinary Attributes', 30 => 'Nutritional Attributes'], null, array('placeholder' => 'Select Feature Type','class' => 'form-control  required')) !!}
        </div>
    </div>
    <!--<div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>¿En qué año se encuentra la característica?:</strong>
            {!! Form::select('year', array(0 => 'Sin Año', 1 => '1º Año' , 2 => '2º Año', 3 => '3º Año', 4 => '4º Año'), null , array('placeholder' => 'Seleccione Año','class' => 'form-control  required')) !!}
        </div>
    </div>-->
    <input type="hidden" name="year" value="0">
</div>
<hr>
<span class="m-section__sub">
    Values
</span>
<div class="row">
    <div class="col-lg-4">
        {!! Form::select('type', array(1 => 'Qualification', 2 => 'Yes or No',3 => 'Value' ), null , array('placeholder' => 'Select value type','class' => 'form-control  required')) !!} 
    </div>
    <div class="col-lg-4">
        {!! Form::select('repeat_do', array(0 => 'Does not repeat', 1 => 'Repeatable' ), null , array('placeholder' => 'Select to enable repetitions','class' => 'form-control  required')) !!} 
    </div>
    <div class="col-lg-2">
        <button type="button" id="new-value" class="btn btn-success"><i class="fa fa-plus"></i> Add additional value</button>
    </div>
</div>     
<br>

<div class="row" id="elements-value">
    
</div>
<!--
<div class="row">
    
</div> 
-->

<script>
    $(document).ready(function(){

        var values = 1;

        $("select[name=type]").change(function(){
            $("#elements-value").html("")
            if($(this).val() == 1 || $(this).val() == 2){
                $("#new-value").removeAttr("disabled")
            }else{
                $("#new-value").attr("disabled","disabled")
            }
        })

        $("#new-value").click(function(){
            
            if($("select[name=type]").val() == 1 || $("select[name=type]").val() == 2 && values <= 2){
                $.ajax({
                    url: '/features/add-value',
                    type: 'GET',
                    dataType: 'html',
                    data: {id: values++},
                }).done(function(data) {
                    $("#elements-value").append(data)
                })
            }
            
        })
    })
</script>