<div class="col-lg-12">
	<div class="col-lg-3">
		<h6>Name {{ $id }}</h6>
		<div class="form-group">
				{!! Form::text('values['.$id.'][name]', null, array('placeholder' => 'Enter Name','class' => 'form-control  required')) !!}
		</div>
	</div>
	<div class="col-lg-3">
		<h6>Value {{ $id }}</h6>
			<div class="form-group">
				{!! Form::text('values['.$id.'][value]', $id, array('placeholder' => 'Enter Value','class' => 'form-control   required')) !!}
			</div>
	</div>
</div>