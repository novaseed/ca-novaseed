
<form id="saveCharacteristics" action="saveCharacteristics" method="POST">
	<input type="hidden" name="crossing_id" value="<?= $crossing->id;?>">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th width="100px">Value</th>
			</tr>
		</thead>
		<tbody>
			@foreach($typesfeatures as $t)
				<tr>
					<td colspan="6"><b>{{ $t->name }}</b></td>
				</tr>
				@foreach($t->features as $f)
					<tr>
						<td>

								{{ $f->name }} 
						</td>
						<td>
							<input type="hidden" name="feature[0][<?= $f->id;?>][feature_id]" value="<?= $f->id;?>">
							@php $crossingFeature = $crossing->characteristics->firstWhere('feature_id', $f->id) @endphp
							@if($f->type == 1)
									<select name="feature[0][<?= $f->id;?>][value]">
										<option value="" disabled <?=!isset($crossingFeature) ? 'selected' : null ?>>Select value</option>
										@foreach($f->values as $value)
											<option value="<?=$value->value?>" <?=isset($crossingFeature) && $crossingFeature->value === $value->value ? 'selected' : null?>><?=$value->name?></option>
										@endforeach
									</select>
								@elseif($f->type == 2)
								<input type="radio" id="yes<?= $f->id;?>" name="feature[0][<?= $f->id;?>][value]" value="1" <?=isset($crossingFeature) && $crossingFeature->value == 1 ? 'checked' : null?>>
							<label for="yes<?= $f->id;?>">Yes</label>
							<input style="margin-left: 1em" type="radio" id="no<?= $f->id;?>" name="feature[0][<?= $f->id;?>][value]" value="2" <?=isset($crossingFeature) && $crossingFeature->value == 2 ? 'checked' : null?>>
							<label for="no<?= $f->id;?>">No</label>
								@elseif($f->type == 3)
									<input type="text" class="fullsize" name="feature[0][<?= $f->id;?>][value]" value="<?= isset($crossingFeature) && isset($crossingFeature->value) ? $crossingFeature->value : null?>">
								@endif
						</td>
					</tr>
				@endforeach
			@endforeach
		</tbody>
	</table>
	<div class="row">
		<div class="col-lg-12">
			<button type="submit" class="btn btn-success" style="float:right"><i class="fa fa-save"></i> Save Characteristics</button>
		</div>
	</div>
</form>