
<fieldset>
    <legend>License Issued</legend>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Organization</strong>
                {!! Form::text('financialInfo[organization]', null, array('placeholder' => 'Enter organization','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Segment</strong>
                {!! Form::text('financialInfo[segment]', null, array('placeholder' => 'Enter segment','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Year</strong>
                {!! Form::text('financialInfo[license_year]', null, array('placeholder' => 'Enter year','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Region</strong>
                {!! Form::text('financialInfo[region]', null, array('placeholder' => 'Enter region','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Royalty Rate ($/t)</strong>
                {!! Form::text('financialInfo[royalty_rate]', null, array('placeholder' => 'Enter royalty rate','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Quantity sold (t)</strong>
                {!! Form::text('financialInfo[quantity_sold]', null, array('placeholder' => 'Enter quantity sold','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Royalty Owed</strong>
                {!! Form::text('financialInfo[royalty_owed]', null, array('placeholder' => 'Enter royalty owed','class' => 'form-control ')) !!}
            </div>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Seed Sale</legend>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Partnership</strong>
                {!! Form::text('financialInfo[partnership]', null, array('placeholder' => 'Enter partnership','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Year</strong>
                {!! Form::text('financialInfo[seed_year]', null, array('placeholder' => 'Enter year','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Product</strong>
                {!! Form::text('financialInfo[product]', null, array('placeholder' => 'Enter product','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Price/unit ($/t)</strong>
                {!! Form::text('financialInfo[price_per_unit]', null, array('placeholder' => 'Enter price/unit','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Sales</strong>
                {!! Form::text('financialInfo[sales]', null, array('placeholder' => 'Enter sales','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Ware Price</strong>
                {!! Form::text('financialInfo[ware_price]', null, array('placeholder' => 'Enter ware price','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Ware Sales</strong>
                {!! Form::text('financialInfo[ware_sales]', null, array('placeholder' => 'Enter ware sales','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Date of Reporting</strong>
                {!! Form::text('financialInfo[date_of_reporting]', null, array('placeholder' => 'Enter date of reporting','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4">
            <div class="form-group">
                <strong>Value per ha</strong>
                {!! Form::text('financialInfo[value_per_ha]', null, array('placeholder' => 'Enter value per ha','class' => 'form-control ')) !!}
            </div>
        </div>
    </div>
</fieldset>

<script>
    $(document).ready(function(){
        $("select").select2();
        $('#rating').select2({ minimumResultsForSearch: Infinity });
        
    })
</script>