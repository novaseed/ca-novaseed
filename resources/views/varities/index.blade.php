@extends('layouts.app')
@section('content')
<div class="m-portlet m-portlet--head-solid-bg m-portlet--rounded">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-calendar"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Varieties
                </h3>
            </div>          
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="{{ route('varities.create') }}" class="m-portlet__nav-link btn btn-outline-primary m-btn m-btn--outline-2x ">
                        Add Varieties
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <table class="table table-striped- table-bordered table-hover table-checkable datatables_p" id="table-cross">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Mother</th>
                    <th>Father</th>
                    <th>Market</th>
                    <th style="width:350px"></th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($varities))
                    @foreach($varities as $record)
                        <tr>
                            <td>{{ $record->id }}</td>
                            <td><b>{{ $record->name }}</b></td>
                            <td><b>{{ @$record->motherName() }}</b></td>
                            <td><b>{{ @$record->fatherName() }}</b></td>
                            <td>
                                <b>
                                    @if($record->marketold_id == $record->market_id)
                                        {{ @$record->market->name }}
                                    @else
                                        Original: {{ @$record->marketold->name }}, Modificado: {{ @$record->market->name }}
                                    @endif
                                </b>
                            </td>
                            <td>
                                <center>
                                   <a class="btn btn-outline-info btn-sm" href="{{ route('varities.show',$record->id) }}">View</a>
                                    <a class="btn btn-outline-success btn-sm " href="{{ route('varities.edit',$record->id) }}">Edit</a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['varities.destroy', $record->id],'style'=>'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-outline-danger btn-sm btn-delete']) !!}
                                    {!! Form::close() !!}
                                </center>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".table").DataTable()
        $(".btn-delete").click(function(){
                    var element = $(this)
                    swal(
                        {
                            title:"Are you sure you want to delete?",
                            text:"This action cannot be reverted.",
                            type:"warning",
                            showCancelButton:!0,
                            confirmButtonText:"Yes, delete",
                            cancelButtonText:"No, cancel",
                            reverseButtons:!0
                        }
                    ).then(function(e){
                        if(e.value){
                           $(element).parent().submit()
                        }else{
                            return false;
                        }
                    })
                    return false;
                })

    });
</script>
@endsection
