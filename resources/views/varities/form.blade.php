<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Name</strong>
            {!! Form::text('name', null, array('placeholder' => 'Type name of Variety','class' => 'form-control required ')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Year</strong>
            {!! Form::text('year', null, array('placeholder' => 'Type Year of Crossing','class' => 'form-control  required')) !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Market</strong>
            {!! Form::select('market_id', $markets,null,['class' => 'form-control required','placeholder' => 'Select Market']); !!}
        </div>
    </div>
     <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Sex</strong>
            {!! Form::select('sex', $sex,null,['class' => 'form-control required','placeholder' => 'Select Sex']); !!}
        </div>
    </div>
     <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Variety Type</strong>
            {!! Form::select('type', $type_varity,null,['class' => 'form-control required','placeholder' => 'Select Type of Variety']); !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Selected Status</strong>
            {!! Form::select('selected_status', [
                'Selected' => 'Selected',
                'rejected' => 'rejected',
                'kept for breeding' => 'kept for breeding'
            ],null,['class' => 'form-control','placeholder' => 'Select status']); !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Potential Usage</strong>
            {!! Form::select('potential_usage', [
                'Fresh market' => 'Fresh market',
                'Organic processing' => 'Organic processing',
                'Starch' => 'Starch',     
                'Dehydrate' => 'Dehydrate',        
                'Colour' => 'Colour',     
                'other' => 'other'
            ],null,['class' => 'form-control','placeholder' => 'Select potential usage']); !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Used in Cross</strong>
            <br/>
            {!! Form::radio('used_in_cross', 1 , false, ['id' => 'crossyes']); !!}
            {!! Form::label('crossyes', 'Yes'); !!}
            {!! Form::radio('used_in_cross', 2 , false, ['id' => 'crossno']); !!}
            {!! Form::label('crossno', 'No'); !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>Stage of Development</strong>
            {!! Form::select('stage_of_development', [
                'Evaluation' => 'Evaluation',
                'Show plot' =>                 'Show plot',
                'Regional Trials' =>                 'Regional Trials',
                'Commercial trials' =>                 'Commercial trials',
                'Tissue culture' =>                 'Tissue culture',
                'TGA' =>                 'TGA',
                'Disease Testing' =>                 'Disease Testing',
                'Registration' =>                 'Registration',
            ],null,['class' => 'form-control','placeholder' => 'Select development stage']); !!}
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <div class="form-group">
            <strong>5 Star Rating</strong>
            {!! Form::select('five_star_rating', [
                1 => '⭐',
                2 => '⭐⭐',
                3 => '⭐⭐⭐',
                4 => '⭐⭐⭐⭐',
                5 => '⭐⭐⭐⭐⭐'
            ],null,['class' => 'form-control','placeholder' => 'Select rating', 'id' => 'rating']); !!}
        </div>
    </div>
</div>

<fieldset>
    <legend>Parent Info</legend>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="form-group">
                <strong>Mother</strong>
                {!! Form::select('mother_id', $varities,null,['class' => 'form-control ','placeholder' => 'Select Variety']); !!}
            </div>
        </div>
        <div class="col-lg-1">
            <button type="button" class="btn btn-warning find-btn" style="margin-top: 19px"><i class="fa fa-search"></i></button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="form-group">
                <strong>Father</strong>
                {!! Form::select('father_id', $varities,null,['class' => 'form-control ','placeholder' => 'Select Variety']); !!}
            </div>
        </div>
        <div class="col-lg-1">
            <button type="button"  class="btn btn-warning find-btn" style="margin-top: 19px"><i class="fa fa-search"></i></button>
        </div>
    </div>
</fieldset>


<script>
    $(document).ready(function(){
        $("select").select2();
        $('#rating').select2({ minimumResultsForSearch: Infinity });
        
    })
</script>