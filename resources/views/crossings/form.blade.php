<input type="hidden" name="crossing" value="1">
<div id="content-cruce">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="form-group">
                <strong>Mother</strong>
                {!! Form::select('mother_id', $varities,null,['class' => 'form-control required','placeholder' => 'Select Variety', 'id' => 'select_mother']); !!}
            </div>
        </div>
        <div class="col-lg-1">
            <button type="button" class="btn btn-warning find-btn" attr-selector="mother" style="margin-top: 19px"><i class="fa fa-search"></i></button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="form-group">
                <strong>Father</strong>
                {!! Form::select('father_id', $varities,null,['class' => 'form-control required','placeholder' => 'Select Variety', 'id' => 'select_father']); !!}
            </div>
        </div>
        
        <div class="col-lg-1">
            <button type="button"  class="btn btn-warning find-btn"  attr-selector="father" style="margin-top: 19px"><i class="fa fa-search"></i></button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2">
            <div class="form-group">
                <strong>Target Market</strong>
                {!! Form::select('market_id', $markets,null,['class' => 'form-control required','placeholder' => 'Select Market']); !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-2">
            <div class="form-group">
                <strong>Year:</strong>
                {!! Form::text('year', null, array('placeholder' => 'Enter crossing year','class' => 'form-control ')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3">
            <div class="form-group">
                <strong>Berries:</strong>
                {!! Form::text('berries_objective', null, array('placeholder' => 'Enter target berries','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-9">
            <div class="form-group">
                <strong>Crossing target:</strong>
                {!! Form::textarea('observation', null, array('placeholder' => '','class' => 'form-control')) !!}
            </div>
        </div>
    </div>
</div>
<!--
<div class="row">
    
</div> 
-->

<script>
    $(document).ready(function(){
        $("input[name=cruce]").click(function(){
            if ($("input[name=cruce][value=1]").is(":checked")) {
                $("#content-cruce").show()
            }else{
                 $("#content-cruce").hide()
            }
        })

        $("select").select2();
    })
</script>