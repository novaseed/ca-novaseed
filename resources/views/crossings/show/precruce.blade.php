<div class="row">
	<div class="col-lg-3">
		<div class="item">
			<h6>Season {{ $crossing->id }}</h6>
			<h5>T{{ $crossing->year }}-{{ $crossing->year+1 }}</h5>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="item">
			<h6>Mother</h6>
			<h5><a href="/varity/{{ $crossing->mother->id }}"><?= ($crossing->mother->crossing == 1)? $crossing->mother->code:$crossing->mother->name;?></a></h5>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="item">
			<h6>Father</h6>
			<h5><a href="/varity/{{ $crossing->father->id }}"><?= ($crossing->father->crossing == 1)? $crossing->father->code:$crossing->father->name;?></a></h5>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="item">
			<h6>Stage</h6>
			<span class="m-badge m-badge--warning m-badge--wide">Year {{ date('Y')-$crossing->year }}</span>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="item">
			<h6>Condition</h6>
			@if($crossing->typecrossing == 1)
                <span class="m-badge m-badge--info m-badge--wide">To be encoded</span>
            @else
                <span class="m-badge m-badge--success m-badge--wide">Encoded</span>
            @endif
		</div>
	</div>
	<div class="col-lg-3">
		<div class="item">
			<h6>Created by</h6>
			<h5>{{ $crossing->user->name }}</h5>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="item">
			<h6>Coded Number</h6>
			<h5>{{ $crossing->codify }}</h5>
		</div>
	</div>
    <div class="col-lg-3">
        <div class="item">
            <h6>Target Market</h6>
            <p>{{ $crossing->market->name }}</p>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="item">
            <h6>Cross Target</h6>
            <p>{{ $crossing->observation }}</p>
        </div>
    </div>
</div>


<hr>
<br>
<div class="row">
    <div class="col-lg-6">
        <fieldset>
            <legend>Characteristics Summary</legend>
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th>AVG</th>
                        <th>No.</th>
                        <th>STD</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($typesfeatures as $t)
                        <tr>
                            <td colspan="6"><b>{{ $t->name }}</b></td>
                        </tr>
                        @foreach($t->features as $f)
                            
                            <!--  Función para formulas de numeros - promedio - desviación -->
                            @include('crossings.show.summary',['feature' => $f,'tree' => $tree])     
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </fieldset>
    </div>
    <div class="col-lg-6">
        <fieldset>
            <legend>Family tree</legend>
            <div class="hv-container">
                <div class="hv-wrapper">

                    <!-- Key component -->
                    <div class="hv-item">

                        <div class="hv-item-parent">

                            <div class="hv-item-child">
                                <p class="simple-card">
                                    <a href="<?= ($crossing->mother->crossing == 1)? "/crossings/".$crossing->mother->id:"/varities/".$crossing->mother->id;?>">
                                        {{ $crossing->motherName() }}    
                                    </a>
                                </p>
                            </div>

                            <div class="hv-item-child">
                                <p class="simple-card">
                                    <a href="<?= ($crossing->father->crossing == 1)? "/crossings/".$crossing->father->id:"/varities/".$crossing->father->id;?>">
                                        {{ $crossing->fatherName() }}  
                                    </a>
                                </p>
                            </div>

                        </div>

                        <div class="hv-item-children">

                            <!--<div class="hv-item-child">
                                <div class="hv-item">

                                    <div class="hv-item-parent">
                                        <p class="simple-card"> Parent </p>
                                    </div>

                                    <div class="hv-item-children">

                                        <div class="hv-item-child">
                                            <p class="simple-card"> Child 1 </p>
                                        </div>

                                    </div>

                                </div>
                            </div>-->

                            @foreach($tree as $t)
                                <div class="hv-item-child">
                                    <p class="simple-card <?= ($t->id == $crossing->id)? 'same':'';?>">
                                        <a href="/crossings/{{ $t->id }}">{{ $t->code }}</a>
                                    </p>
                                </div>
                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
        </fieldset>
    </div>
</div>


<script>
    $(document).ready(function(){
        $("#FormAddPhoto").validate({
            rules: {
                file: {
                  required: true,
                  extension: "jpg|jpeg|png"
                }
            },
            messages: {
                file: {
                  extension: "You must enter only multimedia with format: jpg, png."
                }
            }
        })
    })
</script>