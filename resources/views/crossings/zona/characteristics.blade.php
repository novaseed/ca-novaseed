

<form id="saveCharacteristics" action=/crossings/saveCharacteristics method="POST">
	<input type="hidden" name="zonaltrial_id" value="<?= $zonaltrial->id;?>">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<table class="table">
		<thead>
			<tr>
				<th></th>
				<th width="80px">Year 0</th>
				<th width="80px">Year 1</th>
				<th width="80px">Year 2</th>
				<th width="80px">Year 3</th>
				<th width="80px">Year 4</th>
			</tr>
		</thead>
		<tbody>
			@foreach($typesfeatures as $t)
				<tr>
					<td colspan="6"><b>{{ $t->name }}</b></td>
				</tr>
				@foreach($t->features as $f)
					<tr>
						<td>

								{{ $f->name }} 

							<span style="float:right" class="m-badge m-badge--info m-badge--wide">
								@if($f->type == 1)
									Qualification
								@elseif($f->type == 2)
									Yes/No
								@elseif($f->type == 3)
									Value
								@elseif($f->type == 4)
									Text
								@elseif($f->type == 5)
									Matrix
								@endif
							</span>
						</td>
						<td>
							@if (\strpos($f->year, '0') !== false OR \strpos($f->year, '0') !== false)
								<input type="hidden" name="feature[0][<?= $f->id;?>][feature_id]" value="<?= $f->id;?>">
								<input <?= ($f->type == 1)? 'maxlength="1"':'';?> type="text" class="fullsize" name="feature[0][<?= $f->id;?>][value]" value="<?= $f->valor0;?>">
							@endif
						</td>
						<td>
							@if (\strpos($f->year, '0') !== false OR \strpos($f->year, '1') !== false)
								<input type="hidden" name="feature[1][<?= $f->id;?>][feature_id]" value="<?= $f->id;?>">
								<input <?= ($f->type == 1)? 'maxlength="1"':'';?> type="text" class="fullsize" name="feature[1][<?= $f->id;?>][value]" value="<?= $f->valor1;?>">
							@endif
						</td>
						<td>
							@if (\strpos($f->year, '0') !== false OR \strpos($f->year, '2') !== false)
								<input type="hidden" name="feature[2][<?= $f->id;?>][feature_id]" value="<?= $f->id;?>">
								<input <?= ($f->type == 1)? 'maxlength="1"':'';?> type="text" class="fullsize" name="feature[2][<?= $f->id;?>][value]" value="<?= $f->valor2;?>">
							@endif
						</td>
						<td>
							@if (\strpos($f->year, '0') !== false OR \strpos($f->year, '3') !== false)
								<input type="hidden" name="feature[3][<?= $f->id;?>][feature_id]" value="<?= $f->id;?>">
								<input <?= ($f->type == 1)? 'maxlength="1"':'';?> type="text" class="fullsize" name="feature[3][<?= $f->id;?>][value]" value="<?= $f->valor3;?>">
							@endif
						</td>
						<td>
							@if (\strpos($f->year, '0') !== false OR \strpos($f->year, '4') !== false)
								<input type="hidden" name="feature[4][<?= $f->id;?>][feature_id]" value="<?= $f->id;?>">
								<input <?= ($f->type == 1)? 'maxlength="1"':'';?> type="text" class="fullsize" name="feature[4][<?= $f->id;?>][value]" value="<?= $f->valor4;?>">
							@endif
						</td>
					</tr>
				@endforeach
			@endforeach
		</tbody>
	</table>
	<div class="row">
		<div class="col-lg-12">
			<button type="submit" class="btn btn-success" style="float:right"><i class="fa fa-save"></i> Save Characteristics</button>
		</div>
	</div>
</form>