<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinancialInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('varity_id')->nullable();
            $table->foreign('varity_id')->references('id')->on('varities')->onDelete('cascade');
            $table->string('organization')->nullable();
            $table->string('segment')->nullable();
            $table->string('license_year')->nullable();
            $table->string('region')->nullable();
            $table->string('royalty_rate')->nullable();
            $table->string('quantity_sold')->nullable();
            $table->string('royalty_owed')->nullable();
            $table->string('partnership')->nullable();
            $table->string('seed_year')->nullable();
            $table->string('product')->nullable();
            $table->string('price_per_unit')->nullable();
            $table->string('sales')->nullable();
            $table->string('ware_price')->nullable();
            $table->string('ware_sales')->nullable();
            $table->string('date_of_reporting')->nullable();
            $table->string('value_per_ha')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('financial_infos');
    }
}
