<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToVarieties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('varities', function (Blueprint $table) {
            $table->string('selected_status')->nullable();
            $table->string('potential_usage')->nullable();
            $table->integer('used_in_cross')->nullable();
            $table->string('stage_of_development')->nullable();
            $table->integer('five_star_rating')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('varities', function (Blueprint $table) {
            $table->dropColumn('selected_status');
            $table->dropColumn('potential_usage');
            $table->dropColumn('used_in_cross');
            $table->dropColumn('stage_of_development');
            $table->dropColumn('five_star_rating');
        });
    }
}
