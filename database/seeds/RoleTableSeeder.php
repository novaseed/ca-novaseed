<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
	    $admin->name = 'admin';
	    $admin->description = 'Administrador';
	    $admin->save();

        $admin = new Role();
        $admin->name = 'user';
        $admin->description = 'Usuario';
        $admin->save();

    }
}
