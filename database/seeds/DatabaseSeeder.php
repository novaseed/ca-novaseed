<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionTableSeeder::class);
        $this->call(ProvinceTableSeeder::class);
        $this->call(CommunityTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(TypefeatureTableSeeder::class);
        $this->call(FeatureTableSeeder::class);
        $this->call(ValueTableSeeder::class);
        $this->call(MarketTableSeeder::class);
        $this->call(VarityTableSeeder::class);
    }
}
