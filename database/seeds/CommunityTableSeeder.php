<?php

use Illuminate\Database\Seeder;
use App\Models\Community;


class CommunityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Community::create(['province_id'=>1,'name'=>'ARICA']);
        Community::create(['province_id'=>1,'name'=>'CAMARONES']);
        Community::create(['province_id'=>2,'name'=>'PUTRE']);
        Community::create(['province_id'=>2,'name'=>'GENERAL LAGOS']);
        Community::create(['province_id'=>3,'name'=>'IQUIQUE']);
        Community::create(['province_id'=>3,'name'=>'ALTO HOSPICIO']);
        Community::create(['province_id'=>3,'name'=>'HUARA']);
        Community::create(['province_id'=>3,'name'=>'CAMIÑA']);
        Community::create(['province_id'=>3,'name'=>'COLCHANE']);
        Community::create(['province_id'=>3,'name'=>'PICA']);
        Community::create(['province_id'=>3,'name'=>'POZO ALMONTE']);
        Community::create(['province_id'=>4,'name'=>'TOCOPILLA']);
        Community::create(['province_id'=>4,'name'=>'MARÍA ELENA']);
        Community::create(['province_id'=>5,'name'=>'CALAMA']);
        Community::create(['province_id'=>5,'name'=>'OLLAGUE']);
        Community::create(['province_id'=>5,'name'=>'SAN PEDRO DE ATACAMA']);
        Community::create(['province_id'=>6,'name'=>'ANTOFAGASTA']);
        Community::create(['province_id'=>6,'name'=>'MEJILLONES']);
        Community::create(['province_id'=>6,'name'=>'SIERRA GORDA']);
        Community::create(['province_id'=>6,'name'=>'TALTAL']);
        Community::create(['province_id'=>7,'name'=>'CHAÑARAL']);
        Community::create(['province_id'=>7,'name'=>'DIEGO DE ALMAGRO']);
        Community::create(['province_id'=>8,'name'=>'COPIAPÓ']);
        Community::create(['province_id'=>8,'name'=>'CALDERA']);
        Community::create(['province_id'=>8,'name'=>'TIERRA AMARILLA']);
        Community::create(['province_id'=>9,'name'=>'VALLENAR']);
        Community::create(['province_id'=>9,'name'=>'FREIRINA']);
        Community::create(['province_id'=>9,'name'=>'HUASCO']);
        Community::create(['province_id'=>9,'name'=>'ALTO DEL CARMEN']);
        Community::create(['province_id'=>10,'name'=>'LA SERENA']);
        Community::create(['province_id'=>10,'name'=>'LA HIGUERA']);
        Community::create(['province_id'=>10,'name'=>'COQUIMBO']);
        Community::create(['province_id'=>10,'name'=>'ANDACOLLO']);
        Community::create(['province_id'=>10,'name'=>'VICUÑA']);
        Community::create(['province_id'=>10,'name'=>'PAIHUANO']);
        Community::create(['province_id'=>11,'name'=>'OVALLE']);
        Community::create(['province_id'=>11,'name'=>'RÍO HURTADO']);
        Community::create(['province_id'=>11,'name'=>'MONTE PATRIA']);
        Community::create(['province_id'=>11,'name'=>'COMBARBALÁ']);
        Community::create(['province_id'=>11,'name'=>'PUNITAQUI']);
        Community::create(['province_id'=>12,'name'=>'ILLAPEL']);
        Community::create(['province_id'=>12,'name'=>'SALAMANCA']);
        Community::create(['province_id'=>12,'name'=>'LOS VILOS']);
        Community::create(['province_id'=>12,'name'=>'CANELA']);
        Community::create(['province_id'=>13,'name'=>'VALPARAÍSO']);
        Community::create(['province_id'=>13,'name'=>'CASABLANCA']);
        Community::create(['province_id'=>13,'name'=>'CONCON']);
        Community::create(['province_id'=>13,'name'=>'JUAN FERNÁNDEZ']);
        Community::create(['province_id'=>13,'name'=>'PUCHUNCAVÍ']);
        Community::create(['province_id'=>13,'name'=>'QUILPUÉ']);
        Community::create(['province_id'=>13,'name'=>'QUINTERO']);
        Community::create(['province_id'=>13,'name'=>'VILLA ALEMANA']);
        Community::create(['province_id'=>13,'name'=>'VIÑA DEL MAR']);
        Community::create(['province_id'=>14,'name'=>'PETORCA']);
        Community::create(['province_id'=>14,'name'=>'LA LIGUA']);
        Community::create(['province_id'=>14,'name'=>'CABILDO']);
        Community::create(['province_id'=>14,'name'=>'PAPUDO']);
        Community::create(['province_id'=>14,'name'=>'ZAPALLAR']);
        Community::create(['province_id'=>15,'name'=>'LOS ANDES']);
        Community::create(['province_id'=>15,'name'=>'SAN ESTEBAN']);
        Community::create(['province_id'=>15,'name'=>'CALLE LARGA']);
        Community::create(['province_id'=>15,'name'=>'RINCONADA']);
        Community::create(['province_id'=>16,'name'=>'SAN FELIPE']);
        Community::create(['province_id'=>16,'name'=>'CATEMU']);
        Community::create(['province_id'=>16,'name'=>'LLAY LLAY']);
        Community::create(['province_id'=>16,'name'=>'PANQUEHUE']);
        Community::create(['province_id'=>16,'name'=>'PUTAENDO']);
        Community::create(['province_id'=>16,'name'=>'SANTA MARÍA']);
        Community::create(['province_id'=>17,'name'=>'QUILLOTA']);
        Community::create(['province_id'=>17,'name'=>'CALERA']);
        Community::create(['province_id'=>17,'name'=>'HIJUELAS']);
        Community::create(['province_id'=>17,'name'=>'LIMACHE']);
        Community::create(['province_id'=>17,'name'=>'LA CRUZ']);
        Community::create(['province_id'=>17,'name'=>'NOGALES']);
        Community::create(['province_id'=>17,'name'=>'OLMUÉ']);
        Community::create(['province_id'=>18,'name'=>'SAN ANTONIO']);
        Community::create(['province_id'=>18,'name'=>'ALGARROBO']);
        Community::create(['province_id'=>18,'name'=>'CARTAGENA']);
        Community::create(['province_id'=>18,'name'=>'EL QUISCO']);
        Community::create(['province_id'=>18,'name'=>'EL TABO']);
        Community::create(['province_id'=>18,'name'=>'SANTO DOMINGO']);
        Community::create(['province_id'=>19,'name'=>'ISLA DE PASCUA']);
        Community::create(['province_id'=>20,'name'=>'RANCAHUA']);
        Community::create(['province_id'=>20,'name'=>'CODEGUA']);
        Community::create(['province_id'=>20,'name'=>'COINCO']);
        Community::create(['province_id'=>20,'name'=>'COLTAUCO']);
        Community::create(['province_id'=>20,'name'=>'DOÑIHUE']);
        Community::create(['province_id'=>20,'name'=>'GRANEROS']);
        Community::create(['province_id'=>20,'name'=>'LAS CABRAS']);
        Community::create(['province_id'=>20,'name'=>'MOSTAZAL']);
        Community::create(['province_id'=>20,'name'=>'MACHALÍ']);
        Community::create(['province_id'=>20,'name'=>'MALLOA']);
        Community::create(['province_id'=>20,'name'=>'OLIVAR']);
        Community::create(['province_id'=>20,'name'=>'PEUMO']);
        Community::create(['province_id'=>20,'name'=>'PICHIDEGUA']);
        Community::create(['province_id'=>20,'name'=>'QUINTA DE TILCOCO']);
        Community::create(['province_id'=>20,'name'=>'RENGO']);
        Community::create(['province_id'=>20,'name'=>'REQUINOA']);
        Community::create(['province_id'=>20,'name'=>'SAN VICENTE']);
        Community::create(['province_id'=>21,'name'=>'SAN FERNANDO']);
        Community::create(['province_id'=>21,'name'=>'CHÉPICA']);
        Community::create(['province_id'=>21,'name'=>'CHIMBARONGO']);
        Community::create(['province_id'=>21,'name'=>'LOLOL']);
        Community::create(['province_id'=>21,'name'=>'NANCAHUA']);
        Community::create(['province_id'=>21,'name'=>'PALMILLA']);
        Community::create(['province_id'=>21,'name'=>'PERALILLO']);
        Community::create(['province_id'=>21,'name'=>'PLACILLA']);
        Community::create(['province_id'=>21,'name'=>'PUMANQUE']);
        Community::create(['province_id'=>21,'name'=>'SANTA CRUZ']);
        Community::create(['province_id'=>22,'name'=>'PICHILEMU']);
        Community::create(['province_id'=>22,'name'=>'LA ESTRELLA']);
        Community::create(['province_id'=>22,'name'=>'LITUECHE']);
        Community::create(['province_id'=>22,'name'=>'MARCHIGUE']);
        Community::create(['province_id'=>22,'name'=>'NAVIDAD']);
        Community::create(['province_id'=>22,'name'=>'PAREDONES']);
        Community::create(['province_id'=>23,'name'=>'CURICÓ']);
        Community::create(['province_id'=>23,'name'=>'TENO']);
        Community::create(['province_id'=>23,'name'=>'ROMERAL']);
        Community::create(['province_id'=>23,'name'=>'MOLINA']);
        Community::create(['province_id'=>23,'name'=>'SAGRADA FAMILIA']);
        Community::create(['province_id'=>23,'name'=>'HUALAÑÉ']);
        Community::create(['province_id'=>23,'name'=>'LICANTÉN']);
        Community::create(['province_id'=>23,'name'=>'VICHUQUÉN']);
        Community::create(['province_id'=>23,'name'=>'RAUCO']);
        Community::create(['province_id'=>24,'name'=>'TALCA']);
        Community::create(['province_id'=>24,'name'=>'PELARCO']);
        Community::create(['province_id'=>24,'name'=>'RÍO CLARO']);
        Community::create(['province_id'=>24,'name'=>'SAN CLEMENTE']);
        Community::create(['province_id'=>24,'name'=>'MAULE']);
        Community::create(['province_id'=>24,'name'=>'SAN RAFAEL']);
        Community::create(['province_id'=>24,'name'=>'EMPEDRADO']);
        Community::create(['province_id'=>24,'name'=>'PENCAHUE']);
        Community::create(['province_id'=>24,'name'=>'CONSTITUCIÓN']);
        Community::create(['province_id'=>24,'name'=>'CUREPTO']);
        Community::create(['province_id'=>25,'name'=>'LINARES']);
        Community::create(['province_id'=>25,'name'=>'YERBAS BUENAS']);
        Community::create(['province_id'=>25,'name'=>'COLBÚN']);
        Community::create(['province_id'=>25,'name'=>'LONGAVÍ']);
        Community::create(['province_id'=>25,'name'=>'PARRAL']);
        Community::create(['province_id'=>25,'name'=>'RETIRO']);
        Community::create(['province_id'=>25,'name'=>'VILLA ALEGRE']);
        Community::create(['province_id'=>25,'name'=>'SAN JAVIER']);
        Community::create(['province_id'=>26,'name'=>'CAUQUENES']);
        Community::create(['province_id'=>26,'name'=>'PELUHUE']);
        Community::create(['province_id'=>26,'name'=>'CHANCO']);
        Community::create(['province_id'=>27,'name'=>'CHILLÁN']);
        Community::create(['province_id'=>27,'name'=>'BULNES']);
        Community::create(['province_id'=>27,'name'=>'CHILLAN VIEJO']);
        Community::create(['province_id'=>27,'name'=>'COBQUECURA']);
        Community::create(['province_id'=>27,'name'=>'COELEMU']);
        Community::create(['province_id'=>27,'name'=>'COIHUECO']);
        Community::create(['province_id'=>27,'name'=>'EL CARMEN']);
        Community::create(['province_id'=>27,'name'=>'NINHUE']);
        Community::create(['province_id'=>27,'name'=>'ÑIQUÉN']);
        Community::create(['province_id'=>27,'name'=>'PEMUCO']);
        Community::create(['province_id'=>27,'name'=>'PINTO']);
        Community::create(['province_id'=>27,'name'=>'PORTEZUELO']);
        Community::create(['province_id'=>27,'name'=>'QUILLÓN']);
        Community::create(['province_id'=>27,'name'=>'QUIRIHUE']);
        Community::create(['province_id'=>27,'name'=>'RANQUIL']);
        Community::create(['province_id'=>27,'name'=>'SAN CARLOS']);
        Community::create(['province_id'=>27,'name'=>'SAN FABIÁN']);
        Community::create(['province_id'=>27,'name'=>'SAN IGNACIO']);
        Community::create(['province_id'=>27,'name'=>'SAN NICOLÁS']);
        Community::create(['province_id'=>27,'name'=>'TREHUACO']);
        Community::create(['province_id'=>27,'name'=>'YUNGAY']);
        Community::create(['province_id'=>28,'name'=>'LOS ANGELES']);
        Community::create(['province_id'=>28,'name'=>'ALTO BIO BIO']);
        Community::create(['province_id'=>28,'name'=>'ANTUCO']);
        Community::create(['province_id'=>28,'name'=>'CABRERO']);
        Community::create(['province_id'=>28,'name'=>'LAJA']);
        Community::create(['province_id'=>28,'name'=>'MULCHÉN']);
        Community::create(['province_id'=>28,'name'=>'NACIMIENTO']);
        Community::create(['province_id'=>28,'name'=>'NEGRETE']);
        Community::create(['province_id'=>28,'name'=>'QUILACO']);
        Community::create(['province_id'=>28,'name'=>'QUILLECO']);
        Community::create(['province_id'=>28,'name'=>'SANTA BÁRBARA']);
        Community::create(['province_id'=>28,'name'=>'SAN ROSENDO']);
        Community::create(['province_id'=>28,'name'=>'TUCAPEL']);
        Community::create(['province_id'=>28,'name'=>'YUMBEL']);
        Community::create(['province_id'=>29,'name'=>'CONCEPCIÓN']);
        Community::create(['province_id'=>29,'name'=>'CHIGUAYANTE']);
        Community::create(['province_id'=>29,'name'=>'CORONEL']);
        Community::create(['province_id'=>29,'name'=>'FLORIDA']);
        Community::create(['province_id'=>29,'name'=>'HUALPÉN']);
        Community::create(['province_id'=>29,'name'=>'HUALQUI']);
        Community::create(['province_id'=>29,'name'=>'LOTA']);
        Community::create(['province_id'=>29,'name'=>'PENCO']);
        Community::create(['province_id'=>29,'name'=>'SAN PEDRO DE LA PAZ']);
        Community::create(['province_id'=>29,'name'=>'SANTA JUANA']);
        Community::create(['province_id'=>29,'name'=>'TALCAHUANO']);
        Community::create(['province_id'=>29,'name'=>'TOMÉ']);
        Community::create(['province_id'=>30,'name'=>'ARAUCO']);
        Community::create(['province_id'=>30,'name'=>'CAÑETE']);
        Community::create(['province_id'=>30,'name'=>'CONTULMO']);
        Community::create(['province_id'=>30,'name'=>'CURANILAHUE']);
        Community::create(['province_id'=>30,'name'=>'LEBU']);
        Community::create(['province_id'=>30,'name'=>'LOS ALAMOS']);
        Community::create(['province_id'=>30,'name'=>'TIRUA']);
        Community::create(['province_id'=>31,'name'=>'ANGOL']);
        Community::create(['province_id'=>31,'name'=>'COLLIPULLI']);
        Community::create(['province_id'=>31,'name'=>'CURACAUTÍN']);
        Community::create(['province_id'=>31,'name'=>'ERCILLA']);
        Community::create(['province_id'=>31,'name'=>'LONQUIMAY']);
        Community::create(['province_id'=>31,'name'=>'LOS SAUCES']);
        Community::create(['province_id'=>31,'name'=>'LUMACO']);
        Community::create(['province_id'=>31,'name'=>'PURÉN']);
        Community::create(['province_id'=>31,'name'=>'REINACO']);
        Community::create(['province_id'=>31,'name'=>'TRAIGUÉN']);
        Community::create(['province_id'=>31,'name'=>'VICTORIA']);
        Community::create(['province_id'=>32,'name'=>'TEMUCO']);
        Community::create(['province_id'=>32,'name'=>'CARAHUE']);
        Community::create(['province_id'=>32,'name'=>'CHOLCHOL']);
        Community::create(['province_id'=>32,'name'=>'CUNCO']);
        Community::create(['province_id'=>32,'name'=>'CURARREHUE']);
        Community::create(['province_id'=>32,'name'=>'FREIRE']);
        Community::create(['province_id'=>32,'name'=>'GALVARINO']);
        Community::create(['province_id'=>32,'name'=>'GORBEA']);
        Community::create(['province_id'=>32,'name'=>'LAUTARO']);
        Community::create(['province_id'=>32,'name'=>'LONCOCHE']);
        Community::create(['province_id'=>32,'name'=>'MELIPEUCO']);
        Community::create(['province_id'=>32,'name'=>'NUEVA IMPERIAL']);
        Community::create(['province_id'=>32,'name'=>'PADRE LAS CASAS']);
        Community::create(['province_id'=>32,'name'=>'PERQUENCO']);
        Community::create(['province_id'=>32,'name'=>'PITRUFQUÉN']);
        Community::create(['province_id'=>32,'name'=>'PUCÓN']);
        Community::create(['province_id'=>32,'name'=>'SAAVEDRA']);
        Community::create(['province_id'=>32,'name'=>'TEODORO SCHMIDT']);
        Community::create(['province_id'=>32,'name'=>'TOLTÉN']);
        Community::create(['province_id'=>32,'name'=>'VILCÚN']);
        Community::create(['province_id'=>32,'name'=>'VILLARICA']);
        Community::create(['province_id'=>33,'name'=>'OSORNO']);
        Community::create(['province_id'=>33,'name'=>'PUERTO OCTAY']);
        Community::create(['province_id'=>33,'name'=>'PURRANQUE']);
        Community::create(['province_id'=>33,'name'=>'PUYEHUE']);
        Community::create(['province_id'=>33,'name'=>'RÍO NEGRO']);
        Community::create(['province_id'=>33,'name'=>'SAN JUAN DE LA COSTA']);
        Community::create(['province_id'=>33,'name'=>'SAN PABLO']);
        Community::create(['province_id'=>34,'name'=>'CALBUCO']);
        Community::create(['province_id'=>34,'name'=>'COCHAMÓ']);
        Community::create(['province_id'=>34,'name'=>'FRESIA']);
        Community::create(['province_id'=>34,'name'=>'FRUTILLAR']);
        Community::create(['province_id'=>34,'name'=>'LOS MUERMOS']);
        Community::create(['province_id'=>34,'name'=>'LLANQUIHUE']);
        Community::create(['province_id'=>34,'name'=>'MAULLÍN']);
        Community::create(['province_id'=>34,'name'=>'PUERTO MONTT']);
        Community::create(['province_id'=>34,'name'=>'PUERTO VARAS']);
        Community::create(['province_id'=>35,'name'=>'CHILOÉ']);
        Community::create(['province_id'=>35,'name'=>'ANCUD']);
        Community::create(['province_id'=>35,'name'=>'CASTRO']);
        Community::create(['province_id'=>35,'name'=>'CURACO DE VÉLEZ']);
        Community::create(['province_id'=>35,'name'=>'CHONCHI']);
        Community::create(['province_id'=>35,'name'=>'DALCAHUE']);
        Community::create(['province_id'=>35,'name'=>'PUQUELDÓN']);
        Community::create(['province_id'=>35,'name'=>'QUEILÉN']);
        Community::create(['province_id'=>35,'name'=>'QUELLÓN']);
        Community::create(['province_id'=>35,'name'=>'QUEMCHI']);
        Community::create(['province_id'=>35,'name'=>'QUINCHAO']);
        Community::create(['province_id'=>36,'name'=>'CHAITÉN']);
        Community::create(['province_id'=>36,'name'=>'FUTALEUFÚ']);
        Community::create(['province_id'=>36,'name'=>'HUALAIHUÉ']);
        Community::create(['province_id'=>36,'name'=>'PALENA']);
        Community::create(['province_id'=>37,'name'=>'COCHRANE']);
        Community::create(['province_id'=>37,'name'=>'O\' HIGGINS']);
        Community::create(['province_id'=>37,'name'=>'TORTEL']);
        Community::create(['province_id'=>38,'name'=>'AYSÉN']);
        Community::create(['province_id'=>38,'name'=>'CISNES']);
        Community::create(['province_id'=>38,'name'=>'GUAITECAS']);
        Community::create(['province_id'=>39,'name'=>'COIHAIQUE']);
        Community::create(['province_id'=>39,'name'=>'LAGO VERDE']);
        Community::create(['province_id'=>40,'name'=>'CHILE CHICO']);
        Community::create(['province_id'=>40,'name'=>'RÍO IBAÑEZ']);
        Community::create(['province_id'=>41,'name'=>'NATALES']);
        Community::create(['province_id'=>41,'name'=>'TORRES DEL PAINE']);
        Community::create(['province_id'=>42,'name'=>'PUNTA ARENAS']);
        Community::create(['province_id'=>42,'name'=>'RÍO VERDE']);
        Community::create(['province_id'=>42,'name'=>'LAGUNA BLANCA']);
        Community::create(['province_id'=>42,'name'=>'SAN GREGORIO']);
        Community::create(['province_id'=>43,'name'=>'PORVENIR']);
        Community::create(['province_id'=>43,'name'=>'PRIMAVERA']);
        Community::create(['province_id'=>43,'name'=>'TIMAUKEL']);
        Community::create(['province_id'=>44,'name'=>'NAVARINO']);
        Community::create(['province_id'=>44,'name'=>'ANTÁRTICA']);
        Community::create(['province_id'=>45,'name'=>'CERRILLOS']);
        Community::create(['province_id'=>45,'name'=>'CERRO NAVIA']);
        Community::create(['province_id'=>45,'name'=>'CONCHALÍ']);
        Community::create(['province_id'=>45,'name'=>'EL BOSQUE']);
        Community::create(['province_id'=>45,'name'=>'ESTACIÓN CENTRAL']);
        Community::create(['province_id'=>45,'name'=>'HUECHURABA']);
        Community::create(['province_id'=>45,'name'=>'INDEPENDENCIA']);
        Community::create(['province_id'=>45,'name'=>'LA CISTERNA']);
        Community::create(['province_id'=>45,'name'=>'LA FLORIDA']);
        Community::create(['province_id'=>45,'name'=>'LA GRANJA']);
        Community::create(['province_id'=>45,'name'=>'LA PINTANA']);
        Community::create(['province_id'=>45,'name'=>'LA REINA']);
        Community::create(['province_id'=>45,'name'=>'LAS CONDES']);
        Community::create(['province_id'=>45,'name'=>'LO BARNECHEA']);
        Community::create(['province_id'=>45,'name'=>'LO ESPEJO']);
        Community::create(['province_id'=>45,'name'=>'LO PRADO']);
        Community::create(['province_id'=>45,'name'=>'MACÚL']);
        Community::create(['province_id'=>45,'name'=>'MAIPÚ']);
        Community::create(['province_id'=>45,'name'=>'ÑUÑOA']);
        Community::create(['province_id'=>45,'name'=>'PEDRO AGUIRRE CERDA']);
        Community::create(['province_id'=>45,'name'=>'PEÑALOLÉN']);
        Community::create(['province_id'=>45,'name'=>'PROVIDENCIA']);
        Community::create(['province_id'=>45,'name'=>'PUDAHUEL']);
        Community::create(['province_id'=>45,'name'=>'QUILICURA']);
        Community::create(['province_id'=>45,'name'=>'QUINTA NORMAL']);
        Community::create(['province_id'=>45,'name'=>'RECOLETA']);
        Community::create(['province_id'=>45,'name'=>'RENCA']);
        Community::create(['province_id'=>45,'name'=>'SAN JOAQUÍN']);
        Community::create(['province_id'=>45,'name'=>'SAN MIGUEL']);
        Community::create(['province_id'=>45,'name'=>'SAN RAMÓN']);
        Community::create(['province_id'=>45,'name'=>'SANTIAGO']);
        Community::create(['province_id'=>45,'name'=>'VITACURA']);
        Community::create(['province_id'=>46,'name'=>'PUENTE ALTO']);
        Community::create(['province_id'=>46,'name'=>'PIRQUE']);
        Community::create(['province_id'=>46,'name'=>'SAN JOSÉ DE MAIPO']);
        Community::create(['province_id'=>47,'name'=>'MELIPILLA']);
        Community::create(['province_id'=>47,'name'=>'MARÍA PINTO']);
        Community::create(['province_id'=>47,'name'=>'CURACAVÍ']);
        Community::create(['province_id'=>47,'name'=>'ALHUÉ']);
        Community::create(['province_id'=>47,'name'=>'SAN PEDRO']);
        Community::create(['province_id'=>48,'name'=>'TALAGANTE']);
        Community::create(['province_id'=>48,'name'=>'EL MONTE']);
        Community::create(['province_id'=>48,'name'=>'ISLA DE MAIPO']);
        Community::create(['province_id'=>48,'name'=>'PADRE HURTADO']);
        Community::create(['province_id'=>48,'name'=>'PEÑAFLOR']);
        Community::create(['province_id'=>49,'name'=>'BUIN']);
        Community::create(['province_id'=>49,'name'=>'CALERA DE TANGO']);
        Community::create(['province_id'=>49,'name'=>'PAINE']);
        Community::create(['province_id'=>49,'name'=>'SAN BERNARDO']);
        Community::create(['province_id'=>50,'name'=>'COLINA']);
        Community::create(['province_id'=>50,'name'=>'LAMPA']);
        Community::create(['province_id'=>50,'name'=>'TIL TIL']);
        Community::create(['province_id'=>51,'name'=>'VALDIVIA']);
        Community::create(['province_id'=>51,'name'=>'CORRAL']);
        Community::create(['province_id'=>51,'name'=>'LANCO']);
        Community::create(['province_id'=>51,'name'=>'LOS LAGOS']);
        Community::create(['province_id'=>51,'name'=>'MAFIL']);
        Community::create(['province_id'=>51,'name'=>'MARIQUINA']);
        Community::create(['province_id'=>51,'name'=>'PAILLACO']);
        Community::create(['province_id'=>51,'name'=>'PANGUIPULLI']);
        Community::create(['province_id'=>52,'name'=>'LA UNION']);
        Community::create(['province_id'=>52,'name'=>'FUTRONO']);
        Community::create(['province_id'=>52,'name'=>'LAGO RANCO']);
        Community::create(['province_id'=>52,'name'=>'RÍO BUENO']);

    }
}
