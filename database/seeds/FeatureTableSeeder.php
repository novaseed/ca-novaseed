<?php

use App\Models\Feature;
use App\Models\Typefeature;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;


class FeatureTableSeeder extends Seeder
{
    
    //type = (1 => Calificacion, 2 => Yes/no , 3 => Value, 4 => Valores igual que calificación pero sin rango de 3 a 9 , 5 => tabla matriz de novaseed)


    public function run()
    {

    	$crop  = Typefeature::where('id', 1)->first();
    	$tuber  = Typefeature::where('id', 2)->first();
        $yield  = Typefeature::where('id', 3)->first();
        $densidad15  = Typefeature::where('id', 4)->first();
        $densidad22  = Typefeature::where('id', 5)->first();
        $densidad  = Typefeature::where('id', 6)->first();
        $storage  = Typefeature::where('id', 7)->first();
        $quality  = Typefeature::where('id', 8)->first();
        $resistances  = Typefeature::where('id', 9)->first();
        $chemical  = Typefeature::where('id', 10)->first();
    	$pigments  = Typefeature::where('id', 11)->first();

        /* ETAPA DE CULTIVO (9 elementos) */

	    $feature = new Feature();
        $feature->name = 'Madurity';
	    $feature->namees = 'Madurez';
	    $feature->type = 1;
	    $feature->year = 0;
	    $feature->slug = Str::slug($feature->name);
	    $feature->typefeature_id = $crop->id;
	    $feature->save();

        $feature = new Feature();
        $feature->name = 'Foliage development';
        $feature->namees = 'Desarrollo de follaje';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Green tubers';
        $feature->namees = 'Tubérculos verdes';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Leaf type';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Flower';
        $feature->namees = 'Flor';
        $feature->type = 2;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Berries';
        $feature->namees = 'Bayas';
        $feature->type = 2;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Sterile male';
        $feature->namees = 'Macho estéril';
        $feature->type = 2;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Sterile female';
        $feature->namees = 'Mujer estéril';
        $feature->type = 2;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Emergence';
        $feature->namees = 'Emergencia';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $crop->id;
        $feature->save();

        //Caracteristica tubérculo  (13 elementos)

        $feature = new Feature();
        $feature->name = 'Size';
        $feature->namees = 'Tamaño';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Size distribution';
        $feature->namees = 'Distribución de tamaño';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Shape';
        $feature->namees = 'Forma';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Shape Remarks';
        $feature->namees = 'Forma plana';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Shape regularity';
        $feature->namees = 'Regularidad de forma';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Eyes';
        $feature->namees = 'Ojos';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Flesh color';
        $feature->namees = 'Color de carne';
        $feature->type = 3;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Flesh color pattern';
        $feature->namees = 'Patrón color de carne';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin color';
        $feature->namees = 'Calidad de color';
        $feature->type = 3;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin color pattern';
        $feature->namees = 'Patrón calidad de color';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin quality';
        $feature->namees = 'Calidad de la piel';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Final impression';
        $feature->namees = 'Impresión final';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin set';
        $feature->namees = 'Conjunto de piel';
        $feature->type = 1;
        $feature->year = "3,4";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $tuber->id;
        $feature->save();

        //Yield (9 elementos)

        $feature = new Feature();
        $feature->name = 'Ton/ha';
        $feature->namees = 'Ton/ha';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Relative yield';
        $feature->namees = 'Rendimiento relativo';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Tubers Plant';
        $feature->namees = 'Tubérculos de la planta';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        /*$feature = new Feature();
        $feature->name = 'Size distribution';
        $feature->namees = 'Distribución de tamaño';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();*/

        $feature = new Feature();
        $feature->name = '>65';
        $feature->namees = '>65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '55-65';
        $feature->namees = '55-65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '45-55';
        $feature->namees = '45-55';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '35-45';
        $feature->namees = '35-45';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '25-35';
        $feature->namees = '25-35';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $yield->id;
        $feature->save();

        // Densidad 15 (5 elementos)

        $feature = new Feature();
        $feature->name = '>65';
        $feature->namees = '>65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad15->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '55-65';
        $feature->namees = '55-65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad15->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '45-55';
        $feature->namees = '45-55';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad15->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '35-45';
        $feature->namees = '35-45';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad15->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '25-35';
        $feature->namees = '25-35';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad15->id;
        $feature->save();

        // Densidad 22 (5 elementos)

        $feature = new Feature();
        $feature->name = '>65';
        $feature->namees = '>65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad22->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '55-65';
        $feature->namees = '55-65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad22->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '45-55';
        $feature->namees = '45-55';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad22->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '35-45';
        $feature->namees = '35-45';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad22->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '25-35';
        $feature->namees = '25-35';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad22->id;
        $feature->save();

        // Densidad Std (5 elementos)

        $feature = new Feature();
        $feature->name = '>65';
        $feature->namees = '>65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '55-65';
        $feature->namees = '55-65';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '45-55';
        $feature->namees = '45-55';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '35-45';
        $feature->namees = '35-45';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = '25-35';
        $feature->namees = '25-35';
        $feature->type = 1;
        $feature->year = 0;
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $densidad->id;
        $feature->save();

        // Storage Stage (1 elemento)

        $feature = new Feature();
        $feature->name = 'Dormancy';
        $feature->namees = 'Dormancia';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $storage->id;
        $feature->save();

        // Quality Stage (16 elemento)

        $feature = new Feature();
        $feature->name = 'Dry matter';
        $feature->namees = 'Dry matter';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Frying French fries (8°C)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Frying French fries (<8°C)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Frying crisps (8°C)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Frying crisps (<8°C)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Cooking type';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Discoloration after cooking';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Flesh color';
        $feature->namees = '';
        $feature->type = 3;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Flesh color quality (1g-3b)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin quality';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin color after c.';
        $feature->namees = '';
        $feature->type = 3;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin color quality after c. (1g-3b)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Flesh color';
        $feature->namees = '';
        $feature->type = 3;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Flesh color quality (1g-3b)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Skin cracks (1g-3b)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Wrinkles (1g-3b)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $quality->id;
        $feature->save();


        // Resistances (23 elemento)

        $feature = new Feature();
        $feature->name = 'PVY';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'PVX';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'PLRV';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Globodera Rostochiensis';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Globodera Pallida';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Common scab';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Powdery scab';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Silver scuf';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Pectobacterium';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Late blight';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Early blight';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Fusarium';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Rizoctonia';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Cracking';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Hollow heart';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Little tubers (submarinism)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'IBS (internal brown spot)';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();



        $feature = new Feature();
        $feature->name = 'Bruising';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Drought toletrance';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Heat tolerance';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Salt tolerance';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Harvest Damage';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();


        $feature = new Feature();
        $feature->name = 'Easy to topkill';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $resistances->id;
        $feature->save();

         // Chemical (3 elemento)

        $feature = new Feature();
        $feature->name = 'Sencor';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $chemical->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'CIPC';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $chemical->id;
        $feature->save();

        $feature = new Feature();
        $feature->name = 'Other';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $chemical->id;
        $feature->save();

         // Pigments (1 elemento)

        $feature = new Feature();
        $feature->name = 'Value';
        $feature->namees = '';
        $feature->type = 1;
        $feature->year = "3,4,5";
        $feature->slug = Str::slug($feature->name);
        $feature->typefeature_id = $pigments->id;
        $feature->save();
    }
}
