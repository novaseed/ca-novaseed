<?php

use Illuminate\Database\Seeder;
use App\Models\Feature;
use App\Models\Value;

class ValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$feature  = Feature::where('slug', 'madurity')->first();

	    $value = new Value();
	    $value->name = 'Very early maturing (as Perline/Atlantic)';
	    $value->value = 8.5;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very late maturing (Trauko)';
	    $value->value = 4.5;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'foliage-development')->first();

	    $value = new Value();
	    $value->name = 'Strong and good covering foliage';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Weak, open and low foliage';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

       	


	    $feature  = Feature::where('slug', 'green-tubers')->first();

	    $value = new Value();
	    $value->name = 'No';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Little';
	    $value->value = 7;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Moderate';
	    $value->value = 5;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Lot of';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();
	    
	    

	    $feature  = Feature::where('slug', 'leaf-type')->first();

	    $value = new Value();
	    $value->name = 'Nice flat leaves, superficial veins';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Mozaik type, deep veins, shining leaves';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'emergence')->first();

	    $value = new Value();
	    $value->name = 'Fast and regular';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Retarded and irregular';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();



	    $feature  = Feature::where('slug', 'size')->first();

	    $value = new Value();
	    $value->name = 'Big';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Small';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    
	    $feature  = Feature::where('slug', 'size-distribution')->first();

	    $value = new Value();
	    $value->name = 'Uniform';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Non uniform';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'shape')->first();

	    $value = new Value();
	    $value->name = 'Round';
	    $value->value = "R";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Round oval';
	    $value->value = "RO";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Oval';
	    $value->value = "O";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Oval long';
	    $value->value = "OL";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Long oval';
	    $value->value = "LO";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Long';
	    $value->value = "L";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fingerling short';
	    $value->value = "FS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fingerling long';
	    $value->value = "FL";
	    $value->feature_id = $feature->id;
	    $value->save();


	    $feature  = Feature::where('slug', 'shape-remarks')->first();

	    $value = new Value();
	    $value->name = 'Flat';
	    $value->value = "F";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Pear';
	    $value->value = "P";
	    $value->feature_id = $feature->id;
	    $value->save();


	    $feature  = Feature::where('slug', 'shape-regularity')->first();

	    $value = new Value();
	    $value->name = 'Regular';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Non regular';
	    $value->value = 5;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'eyes')->first();

	    $value = new Value();
	    $value->name = 'Smooth';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Deep';
	    $value->value = 5;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'flesh-color-pattern')->first();

	    $value = new Value();
	    $value->name = 'Uniform';
	    $value->value = "U";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Eyes';
	    $value->value = "E";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Eyebrows';
	    $value->value = "EB";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Splashed';
	    $value->value = "SPL";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Scattered';
	    $value->value = "SCA";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Spectacled';
	    $value->value = "SPE";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Stippled';
	    $value->value = "STI";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Blended';
	    $value->value = "B";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'skin-color-pattern')->first();

	    $value = new Value();
	    $value->name = 'Uniform';
	    $value->value = "U";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Scattered Spots';
	    $value->value = "SS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Narrow Vascular Ring';
	    $value->value = "NVR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Broad Vascular Ring';
	    $value->value = "BVR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Vascular Ring medulla';
	    $value->value = "VRM";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'All Flesh except Medulla';
	    $value->value = "FEM";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'skin-quality')->first();

	    $value = new Value();
	    $value->name = 'Brillant, smoth';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Opaque, netty';
	    $value->value = 4;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Russet type';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'final-impression')->first();

	    $value = new Value();
	    $value->name = 'Very good';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();


	    $feature  = Feature::where('slug', 'skin-set')->first();

	    $value = new Value();
	    $value->name = 'Good fast';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'slow bad';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();


	    $feature  = Feature::where('slug', 'dormancy')->first();

	    $value = new Value();
	    $value->name = 'Abril';
	    $value->value = 4;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Mayo';
	    $value->value = 5;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Junio';
	    $value->value = 6;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Julio';
	    $value->value = 7;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Agosto';
	    $value->value = 8;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Septiembre';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Octubre';
	    $value->value = 10;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Noviembre';
	    $value->value = 11;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Diciembre';
	    $value->value = 12;
	    $value->feature_id = $feature->id;
	    $value->save();


	    //Quality Stage

	    //frying-french-fries-80c

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = 48;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Accepable';
	    $value->value = 7;
	    $value->feature_id = 48;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Excelent';
	    $value->value = 9;
	    $value->feature_id = 48;
	    $value->save();


	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = 49;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Accepable';
	    $value->value = 7;
	    $value->feature_id = 49;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Excelent';
	    $value->value = 9;
	    $value->feature_id = 49;
	    $value->save();

	    //frying-crisps-80c

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = 50;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Accepable';
	    $value->value = 7;
	    $value->feature_id = 50;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Excelent';
	    $value->value = 9;
	    $value->feature_id = 50;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = 51;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Accepable';
	    $value->value = 7;
	    $value->feature_id = 51;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Excelent';
	    $value->value = 9;
	    $value->feature_id = 51;
	    $value->save();

	    $feature  = Feature::where('slug', 'cooking-type')->first();

	    $value = new Value();
	    $value->name = 'Firm';
	    $value->value = "A";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fairly firm';
	    $value->value = "AB";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Firm and some mealy';
	    $value->value = "B";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'More mealy than firm';
	    $value->value = "BC";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Mealy';
	    $value->value = "C";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very mealy';
	    $value->value = "CD";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Stong mealy';
	    $value->value = "D";
	    $value->feature_id = $feature->id;
	    $value->save();


	    $feature  = Feature::where('slug', 'discoloration-after-cooking')->first();

	    $value = new Value();
	    $value->name = 'No discoloration';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Moderate discoloration';
	    $value->value = 7;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Strong discoloration';
	    $value->value = 5;
	    $value->feature_id = $feature->id;
	    $value->save();

	    //flesh-color-quality-1g-3b

	    $value = new Value();
	    $value->name = 'Good';
	    $value->value = 1;
	    $value->feature_id = 55;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = 55;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Good';
	    $value->value = 1;
	    $value->feature_id = 60;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = 60;
	    $value->save();

	    //skin-quality

	    $value = new Value();
	    $value->name = 'Good';
	    $value->value = 1;
	    $value->feature_id = 56;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = 56;
	    $value->save();


	    $feature  = Feature::where('slug', 'skin-color-quality-after-c-1g-3b')->first();

	    $value = new Value();
	    $value->name = 'Good';
	    $value->value = 1;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'flesh-color-quality-1g-3b')->first();

	    $value = new Value();
	    $value->name = 'Good';
	    $value->value = 1;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'skin-cracks-1g-3b')->first();

	    $value = new Value();
	    $value->name = 'Good';
	    $value->value = 1;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'wrinkles-1g-3b')->first();

	    $value = new Value();
	    $value->name = 'Good';
	    $value->value = 1;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Bad';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();


	    //Resistances

	    $feature  = Feature::where('slug', 'pvy')->first();

	    $value = new Value();
	    $value->name = 'Resistant';
	    $value->value = "R";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Medium resistant';
	    $value->value = "MR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fairly susceptible';
	    $value->value = "FS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'susceptible';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'pvx')->first();

	    $value = new Value();
	    $value->name = 'Resistant';
	    $value->value = "R";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Medium resistant';
	    $value->value = "MR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fairly susceptible';
	    $value->value = "FS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'susceptible';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'plrv')->first();

	    $value = new Value();
	    $value->name = 'Resistant';
	    $value->value = "R";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Medium resistant';
	    $value->value = "MR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fairly susceptible';
	    $value->value = "FS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'susceptible';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'globodera-rostochiensis')->first();

	    $value = new Value();
	    $value->name = 'Resistant';
	    $value->value = "R";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Medium resistant';
	    $value->value = "MR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fairly susceptible';
	    $value->value = "FS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'susceptible';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'globodera-pallida')->first();

	    $value = new Value();
	    $value->name = 'Resistant';
	    $value->value = "R";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Medium resistant';
	    $value->value = "MR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fairly susceptible';
	    $value->value = "FS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'susceptible';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'pectobacterium')->first();

	    $value = new Value();
	    $value->name = 'Resistant';
	    $value->value = "R";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Medium resistant';
	    $value->value = "MR";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Fairly susceptible';
	    $value->value = "FS";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'susceptible';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'common-scab')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'powdery-scab')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'silver-scuf')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'late-blight')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'early-blight')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'fusarium')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'cracking')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'hollow-heart')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'little-tubers-submarinism')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'ibs-internal-brown-spot')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'drought-toletrance')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'heat-tolerance')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'salt-tolerance')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'harvest-damage')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'easy-to-topkill')->first();

	    $value = new Value();
	    $value->name = 'Susceptible';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very resistant';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();



	    $feature  = Feature::where('slug', 'sencor')->first();

	    $value = new Value();
	    $value->name = 'Mild';
	    $value->value = "MI";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Moderate';
	    $value->value = "MO";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Severe';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'cipc')->first();

	    $value = new Value();
	    $value->name = 'Mild';
	    $value->value = "MI";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Moderate';
	    $value->value = "MO";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Severe';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $feature  = Feature::where('slug', 'other')->first();

	    $value = new Value();
	    $value->name = 'Mild';
	    $value->value = "MI";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Moderate';
	    $value->value = "MO";
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Severe';
	    $value->value = "S";
	    $value->feature_id = $feature->id;
	    $value->save();

	    
/*
	    $feature  = Feature::where('slug', 'dry-matter')->first();

	    $value = new Value();
	    $value->name = 'Very big';
	    $value->value = 9;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Ca. 50 mm (fingers just around the tubers)';
	    $value->value = 7;
	    $value->feature_id = $feature->id;
	    $value->save();

	    $value = new Value();
	    $value->name = 'Very small';
	    $value->value = 3;
	    $value->feature_id = $feature->id;
	    $value->save();*/

	    


    }
}
