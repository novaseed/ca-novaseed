<?php

use App\Models\Typefeature;
use Illuminate\Database\Seeder;

class TypefeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Typefeature = new Typefeature();
	    $Typefeature->name = 'Crop Stage';
	    $Typefeature->namees = 'Etapa de Cultivo';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Tuber characteristics';
	    $Typefeature->namees = 'Características del tubérculo';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Yield';
	    $Typefeature->namees = 'Rendimiento';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Density (15)';
	    $Typefeature->namees = 'Densidad (15)';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Density (22)';
	    $Typefeature->namees = 'Densidad (22)';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Density (Std)';
	    $Typefeature->namees = 'Densidad (Std)';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Storage Stage';
	    $Typefeature->namees = 'Etapa de almacenamiento';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Quality stage';
	    $Typefeature->namees = 'Etapa de calidad';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Resistances biotic & abiotic stage';
	    $Typefeature->namees = 'Resistencias bióticas y abióticas.';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Chemical Sensitivity';
	    $Typefeature->namees = 'Sensibilidad Química';
	    $Typefeature->save();

	    $Typefeature = new Typefeature();
	    $Typefeature->name = 'Pigments';
	    $Typefeature->namees = 'Pigmentos';
	    $Typefeature->save();

    }
}
