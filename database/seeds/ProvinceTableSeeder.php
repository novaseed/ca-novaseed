<?php

use Illuminate\Database\Seeder;
use App\Models\Province;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::create(['region_id'=>15,'name'=>'ARICA']);
        Province::create(['region_id'=>15,'name'=>'PARINACOTA']);
        Province::create(['region_id'=>1,'name'=>'IQUIQUE']);
        Province::create(['region_id'=>2,'name'=>'TOCOPILLA']);
        Province::create(['region_id'=>2,'name'=>'EL LOA']);
        Province::create(['region_id'=>2,'name'=>'ANTOFAGASTA']);
        Province::create(['region_id'=>3,'name'=>'CHAÑARAL']);
        Province::create(['region_id'=>3,'name'=>'COPIAPÓ']);
        Province::create(['region_id'=>3,'name'=>'HUASCO']);
        Province::create(['region_id'=>4,'name'=>'ELQUI']);
        Province::create(['region_id'=>4,'name'=>'LIMARÍ']);
        Province::create(['region_id'=>4,'name'=>'CHOAPA']);
        Province::create(['region_id'=>5,'name'=>'VALPARAÍSO']);
        Province::create(['region_id'=>5,'name'=>'PETORCA']);
        Province::create(['region_id'=>5,'name'=>'LOS ANDES']);
        Province::create(['region_id'=>5,'name'=>'SAN FELIPE DE ACONCAGUA']);
        Province::create(['region_id'=>5,'name'=>'QUILLOTA']);
        Province::create(['region_id'=>5,'name'=>'SAN ANTONIO']);
        Province::create(['region_id'=>5,'name'=>'ISLA DE PASCUA']);
        Province::create(['region_id'=>6,'name'=>'CACHAPOAL']);
        Province::create(['region_id'=>6,'name'=>'COLCHAHUA']);
        Province::create(['region_id'=>6,'name'=>'CARDENAL CARO']);
        Province::create(['region_id'=>7,'name'=>'CURICÓ']);
        Province::create(['region_id'=>7,'name'=>'TALCA']);
        Province::create(['region_id'=>7,'name'=>'LINARES']);
        Province::create(['region_id'=>7,'name'=>'CAUQUENES']);
        Province::create(['region_id'=>8,'name'=>'ÑUBLE']);
        Province::create(['region_id'=>8,'name'=>'BIO BIO']);
        Province::create(['region_id'=>8,'name'=>'CONCEPCIÓN']);
        Province::create(['region_id'=>8,'name'=>'ARAUCO']);
        Province::create(['region_id'=>9,'name'=>'MALLECO']);
        Province::create(['region_id'=>9,'name'=>'CAUTÍN']);
        Province::create(['region_id'=>10,'name'=>'OSORNO']);
        Province::create(['region_id'=>10,'name'=>'LLANQUIHUE']);
        Province::create(['region_id'=>10,'name'=>'CHILOÉ']);
        Province::create(['region_id'=>10,'name'=>'PALENA']);
        Province::create(['region_id'=>11,'name'=>'CAPITÁN PRATT']);
        Province::create(['region_id'=>11,'name'=>'AYSÉN']);
        Province::create(['region_id'=>11,'name'=>'COIHAIQUE']);
        Province::create(['region_id'=>11,'name'=>'GENERAL CARRERA']);
        Province::create(['region_id'=>12,'name'=>'ÚLTIMA ESPERANZA']);
        Province::create(['region_id'=>12,'name'=>'MAGALLANES']);
        Province::create(['region_id'=>12,'name'=>'TIERRA DEL FUEGO']);
        Province::create(['region_id'=>12,'name'=>'ANTÁRTICA CHILENA']);
        Province::create(['region_id'=>13,'name'=>'SANTIAGO']);
        Province::create(['region_id'=>13,'name'=>'CORDILLERA']);
        Province::create(['region_id'=>13,'name'=>'MELIPILLA']);
        Province::create(['region_id'=>13,'name'=>'TALAGANTE']);
        Province::create(['region_id'=>13,'name'=>'MAIPO']);
        Province::create(['region_id'=>13,'name'=>'CHACABUCO']);
        Province::create(['region_id'=>14,'name'=>'VALDIVIA']);
        Province::create(['region_id'=>14,'name'=>'RANCO']);
    }
}
