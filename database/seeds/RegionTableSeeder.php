<?php

use Illuminate\Database\Seeder;
use App\Models\Region;


class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::create(['name'=>'REGIÓN DE TARAPACÁ']);
        Region::create(['name'=>'REGIÓN DE ANTOFAGASTA']);
        Region::create(['name'=>'REGIÓN DE ATACAMA']);
        Region::create(['name'=>'REGIÓN DE COQUIMBO']);
        Region::create(['name'=>'REGIÓN DE VALPARAISO']);
        Region::create(['name'=>'REGIÓN DEL LIBERTADOR GENERAL BERNARDO O\'HIGGINS']);
        Region::create(['name'=>'REGIÓN DEL MAULE']);
        Region::create(['name'=>'REGIÓN DEL BÍO - BÍO']);
        Region::create(['name'=>'REGIÓN DE LA ARAUCANÍA']);
        Region::create(['name'=>'REGIÓN DE LOS LAGOS']);
        Region::create(['name'=>'REGIÓN AYSÉN DEL GENERAL CARLOS IBÁÑEZ DEL CAMPO']);
        Region::create(['name'=>'REGIÓN DE MAGALLANES Y LA ANTÁRTICA CHILENA ']);
        Region::create(['name'=>'REGIÓN METROPOLITANA']);
        Region::create(['name'=>'REGION DE LOS RÍOS']);
        Region::create(['name'=>'REGIÓN DE ARICA Y PARINACOTA']);

    }
}
