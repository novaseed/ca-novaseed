<?php

use Illuminate\Database\Seeder;
use App\Models\Market;

class MarketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $market = new Market();
	    $market->name = 'Baby';
	    $market->save();

	    $market = new Market();
	    $market->name = 'Bi-Color';
	    $market->save();

	    $market = new Market();
	    $market->name = 'Chips';
	    $market->save();

	    $market = new Market();
	    $market->name = 'FF';
	    $market->save();

	    $market = new Market();
	    $market->name = 'FF/CH';
	    $market->save();

	    $market = new Market();
	    $market->name = 'Table';
	    $market->save();

	    $market = new Market();
	    $market->name = 'Double Propouse';
	    $market->save();

	    $market = new Market();
	    $market->name = 'Organic';
	    $market->save();

	    

    }
}
