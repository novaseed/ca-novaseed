<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_manager  = Role::where('name', 'admin')->first();

	    $manager = new User();
	    $manager->name = 'Jorge Mora';
        $manager->rut = '17.034.398-0';
	    $manager->email = 'info@weblabs.cl';
	    $manager->password = bcrypt('qwaszx');
	    $manager->save();
	    $manager->roles()->attach($role_manager);

        $manager = new User();
        $manager->name = 'Boris Contreras';
        $manager->rut = '8.382.586-3';
        $manager->email = 'papasarcoiris@gmail.com';
        $manager->password = bcrypt('nova');
        $manager->save();
        $manager->roles()->attach($role_manager);

        $manager = new User();
        $manager->name = 'Boris Sepulveda';
        $manager->rut = '15.126.995-8';
        $manager->email = 'papasarcoiris2@gmail.com';
        $manager->password = bcrypt('nova');
        $manager->save();
        $manager->roles()->attach($role_manager);

        $manager = new User();
        $manager->name = 'Sergio Barria';
        $manager->rut = '18.204.838-0';
        $manager->email = 'sbarria@weblabs.cl';
        $manager->password = bcrypt('123456');
        $manager->save();
        $manager->roles()->attach($role_manager);
	   
    }
}
