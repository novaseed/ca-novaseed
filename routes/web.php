<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Feature;

Route::get('/', 'HomeController@index');
Route::get('/import', 'HomeController@import');
Route::post('/import', 'HomeController@importExcel');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/features/add-value', function () {
	return View('features._add-value',['id' => $_GET['id']]);
});

Route::get('/features/get-values', function () {
	$feature = Feature::find($_GET['id']);
	return View('features._get-values',['feature' => $feature]);
});
Route::get('/varities/exporData', 'VarityController@check_db');
Route::get('/varities/searchHome', 'VarityController@searchHome');
Route::get('/varities/searchHomeCrossing', 'VarityController@searchHomeCrossing');
Route::get('/varities/addValues', 'VarityController@addValues');
Route::get('/varities/addFeature', 'VarityController@addFeature');
Route::get('/blocks/addFeature', 'BlockController@addFeature');
Route::post('/varities/saveCharacteristics', 'VarityController@saveCharacteristics');
Route::post('/varities/uploadPhoto', 'VarityController@uploadPhoto');
Route::get('/varities/ancesters/{id}/{maxlevel}/{level}', 'VarityController@ancesters');
Route::put('/varities/financial/{varity}', 'VarityController@financialInfo')->name('varities.financialInfo');
Route::resource('varities', 'VarityController');


Route::get('/crossings/zonashow/{id?}', 'CrossingController@zonashow')->name('crossings.zonashow');
Route::get('/crossings/zonaedit/{id?}', 'CrossingController@zonaedit')->name('crossings.zonaedit');
Route::get('/crossings/zonacreate/{id?}', 'CrossingController@zonacreate')->name('crossings.zonacreate');
Route::post('/crossings/zonacreate', 'CrossingController@zonastore')->name('crossings.zonacreate');
Route::get('/crossings/mapout', 'CrossingController@mapout');
Route::get('/crossings/zona', 'CrossingController@zona');
Route::get('/crossings/saveObservations', 'CrossingController@saveObservations');
Route::post('/crossings/uploadPhoto', 'CrossingController@uploadPhoto');
Route::get('/crossings/deleteCodify', 'CrossingController@deleteCodify');
Route::get('/crossings/generateCodify', 'CrossingController@generateCodify');
Route::get('/crossings/codify/{id}', 'CrossingController@codify');
Route::get('/crossings/finishCodify/{id}', 'CrossingController@finishCodify');
Route::get('/crossings/codedJSON/', 'CrossingController@codedJSON');
Route::get('/crossings/items/', 'CrossingController@items');
Route::post('/crossings/saveCodify/', 'CrossingController@saveCodify');
Route::post('/crossings/saveCharacteristics', 'CrossingController@saveCharacteristics');
Route::get('/crossings/check_ancester/', 'CrossingController@check_ancester');


Route::post('/blocks/saveShowFarms','BlockController@saveShowFarms');
Route::get('/blocks/modCaracteristicas','BlockController@modCaracteristicas');
Route::get('/blocks/saveObservation','BlockController@saveObservation');
Route::get('/blocks/getCaracteristicas','BlockController@getCaracteristicas');	
Route::get('/blocks/getFotos','BlockController@getFotos');
Route::get('/blocks/codify','BlockController@codify');
Route::get('/blocks/print','BlockController@print');
Route::get('/blocks/move_position','BlockController@move_position');
Route::get('/blocks/info_to_compare','BlockController@info_to_compare');
Route::get('/blocks/deleteCodify/{id}','BlockController@deleteCodify');
Route::get('/blocks/getDataChar/','BlockController@getDataChar');
Route::get('/blocks/reorder/{id}','BlockController@reorder');
Route::get('/blocks/discard/','BlockController@discard');
Route::get('/blocks/review/','BlockController@review');
Route::get('/blocks/export/{id}','BlockController@export');
Route::get('/blocks/send_to_blocks','BlockController@send_to_blocks');
Route::get('/blocks/trash','BlockController@trash');
Route::get('/blocks/show_trash/{id}','BlockController@show_trash');
Route::get('/blocks/mark_offline/','BlockController@mark_offline');
Route::resource('blocks', 'BlockController');

Route::get('/mapouts/generateCodify/{id}','MapoutController@generateCodify');
Route::get('/mapouts/deleteCodify/{id}','MapoutController@deleteCodify');
Route::post('/mapouts/updatePosition', 'MapoutController@updatePosition')->name('mapouts.updatePosition');
Route::post('/mapouts/addPositions', 'MapoutController@addPositions')->name('mapouts.addPositions');
Route::resource('mapouts', 'MapoutController');
Route::resource('crossings', 'CrossingController');
Route::resource('features', 'FeatureController');

Route::get("farmers/provinces/", "FarmersController@getProvinces");
Route::get("farmers/communities/", "FarmersController@getCommunities");
Route::get("farmers/lands/", "FarmersController@getLands");

Route::resource('farmers', 'FarmersController');
Route::resource('companies', 'CompanyController');
Route::resource('lands', 'LandController');
Route::resource('traceability', 'TraceabilityController');
Route::get("royalties/create/{farmer_id}", "RoyaltiesController@create");

Route::resource('royalties', 'RoyaltiesController');

Route::get('/results/getData/','ResultsController@getData');
Route::get('/results/crossings/','ResultsController@crossings');
Route::get('/results/fries/','ResultsController@fries');
Route::get('/results/searchCrossings/','CrossingController@searchCrossings');
Route::get('/results/getVaritiesFromBlock/','ResultsController@getVaritiesFromBlock');
Route::get('/results/getChildrensFromVarity/','ResultsController@getChildrensFromVarity');
Route::get('/results/getCaracteristicas/','ResultsController@getCaracteristicas');
Route::get('/results/modCaracteristicas','ResultsController@modCaracteristicas');
Route::any('/fries/save/','BlockController@saveDataFries');			
Route::get('/fries/getData/','BlockController@getDataFries');
Route::any('/fries/updateFries/','BlockController@updateDataFries');			

Route::get('/results/preview_report/','ResultsController@preview_report');
Route::get('/results/load_preview/','ResultsController@load_preview');
Route::any('/results/report/','ResultsController@report');
Route::get('/results/getVarities/','ResultsController@varities');
Route::get('/results/info_to_compare/','ResultsController@info_to_compare');
Route::get('/results/getBlocksByYear/','ResultsController@getBlocksByYear');
Route::get('/results/getVarityByname/','ResultsController@getVarityByname');
Route::get('/results/getVarityBynameInfo/','ResultsController@getVarityBynameInfo');
Route::get('/results/getDataFeature/','ResultsController@getDataFeature');
Route::get('/results/stadistics/','ResultsController@stadistics');

Route::resource('results', 'ResultsController');

Route::resource('std_market', 'StdMarketController');


Route::DELETE('/greenhouse/destroy_greenhouse', 'GreenHouseController@destroy_greenhouse')->name('greenhouse.destroy_greenhouse');
Route::PATCH('/greenhouse/update_greenhouse', 'GreenHouseController@update_greenhouse')->name('greenhouse.update_greenhouse');
Route::get('/greenhouse/asociate_crossing/{id}','GreenHouseController@asociate_crossing')->name('greenhouse.asociate_crossing');
Route::get('/greenhouse/crossings_add/','GreenHouseController@crossings_add');
Route::get('/greenhouse/crossings_added/{id}','GreenHouseController@crossings_added');
Route::post('/greenhouse/store_crossings', 'GreenHouseController@store_crossings');
Route::post('/greenhouse/delete_crossings', 'GreenHouseController@delete_crossings');


Route::get('/greenhouse/generateCodify/{id}','GreenHouseController@generateCodify');
Route::get('/greenhouse/show_mobile/{id}','GreenHouseController@show_mobile');
Route::get('/greenhouse/crossings_ok/{id}','GreenHouseController@crossings_ok');
Route::get('/greenhouse/crossings_list/{id}','GreenHouseController@crossings_list');
Route::get('/greenhouse/show_mobile/{id}','GreenHouseController@show_mobile');
Route::get('/public/draw_greenhouse/{id}','GreenHouseController@draw_greenhouse');
Route::get('draw_greenhouse/{id}','GreenHouseController@draw_greenhouse');
Route::get('/greenhouse/deleteCodify/{id}','GreenHouseController@deleteCodify');
Route::post('/greenhouse/updatePosition', 'GreenHouseController@updatePosition')->name('greenhouse.updatePosition');
Route::post('/greenhouse/addPositions', 'GreenHouseController@addPositions')->name('greenhouse.addPositions');
Route::get('/greenhouse/switch/{varity_id}/{father_id}/{mother_id}','GreenHouseController@switchFathers');
Route::get('/greenhouse/polen/{father_id}/{mother_id}/{status}','GreenHouseController@savePolen');
Route::get('/greenhouse/berries/{id}/{nro_berries}','GreenHouseController@save_berries');
Route::get('/greenhouse/glasses/{id}/{nro_glases}','GreenHouseController@save_glasses');
Route::get('/greenhouse/get_data_chart','GreenHouseController@get_data_chart');
Route::resource('greenhouse', 'GreenHouseController');

Route::get('/relative_yield/calc','RelativeYieldController@calc');

Route::resource('relative_yield', 'RelativeYieldController');

Route::get('/users/logins', 'UserController@logins');
Route::resource('users', 'UserController');
